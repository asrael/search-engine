import { useRef } from 'react';
import Select from 'react-select';
import styled from 'styled-components';

const Container = styled.div`
  border-radius: 24px;
  box-shadow: 0 0 12px -3px rgba(0, 0, 0, 0.28);
  display: flex;
  align-items: center;
  min-width: 300px;
`;

const Label = styled.span`
  cursor: pointer;
  font-weight: bold;
  border-right: 1px solid #ddd;
  padding: 0 16px;
`;

const StyledSelect = styled(Select)`
  flex: 1;
`;

export default function RoundedSelect({ label, ...props }) {
  const refSelect = useRef();
  return (
    <Container>
      {label && (
        <Label
          onClick={() => {
            refSelect?.current?.select?.focus();
          }}
        >
          {label}
        </Label>
      )}
      <StyledSelect
        styles={{
          control: (provided) => ({
            ...provided,
            border: 'none',
            boxShadow: 'none',
            backgroundColor: 'transparent',
            '&:hover': {
              ...provided['&:hover'],
              border: 'none',
            },
          }),
        }}
        {...props}
        openMenuOnFocus
        ref={refSelect}
      />
    </Container>
  );
}
