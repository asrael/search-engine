import styled from 'styled-components';

const Container = styled.span`
  margin: 0 0.5em;
  border-radius: 24px;
  box-shadow: 0 0 12px -3px rgba(0, 0, 0, 0.28);
  border: 1px solid #e4e4e4;
  padding: 0.5em 1em;
  background-color: #eee;
  transition: box-shadow cubic-bezier(0.23, 1, 0.32, 1) 250ms;

  &:hover {
    box-shadow: 0 3px 12px 0px rgba(0, 0, 0, 0.28);
  }

  &:active {
    background-color: #eee;
  }
`;

export default function RoundedText({ children, ...props }) {
  return <Container {...props}>{children}</Container>;
}
