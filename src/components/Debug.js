import { useRouter } from 'next/router';

export default function Debug({ children }) {
  const { query } = useRouter();

  return (
    ((typeof process !== 'undefined' && process.env.NODE_ENV === 'development') ||
      typeof query.debug !== 'undefined') && <>{children}</>
  );
}
