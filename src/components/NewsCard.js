import styled from 'styled-components';
import Link from 'next/link';
import querystring from 'querystring';
import { useRouter } from 'next/router';
import { BiCalendar, BiGlobe, BiPurchaseTag, BiNews } from 'react-icons/bi';

import { uriToId } from '@helpers/utils';
import Debug from '@components/Debug';
import config from '~/src/config';

const Container = styled.div`
  border: 1px solid #ddd;
  border-radius: 8px;
  padding: 1rem;
  margin: 1rem 0;

  h3 a {
    color: #000;
    text-decoration: none;
  }
`;

const RelatedList = styled.ul`
  list-style: none;
  margin-bottom: 0.5rem;

  li:before {
    content: '\\2022';
    color: ${({ theme }) => theme.colors.primary};
    font-weight: bold;
    display: inline-block;
    width: 1em;
  }

  a {
    text-decoration: none;
  }
`;

const Tags = styled.div`
  display: flex;
  margin: 0.25rem 0;
`;

const Tag = styled.div`
  display: flex;
  align-items: center;
  margin-right: 1rem;
`;

const TagIcon = styled.svg`
  margin-right: 0.5rem;
  flex-shrink: 0;
`;

const TagText = styled.span`
  color: #aaa;
`;

export default function NewsCard({ news }) {
  const { query } = useRouter();

  return (
    <Container>
      <h3>
        <Link
          href={`/news/${encodeURIComponent(
            uriToId(news.id, { base: config.newsBaseUri })
          )}?${querystring.stringify({
            s: Buffer.from(querystring.stringify(query)).toString('base64'),
          })}#${uriToId(news.id, { base: config.newsBaseUri })}`}
        >
          <a>{news.headline}</a>
        </Link>
      </h3>
      <Tags>
        {news.dateCreated && (
          <Tag>
            <TagIcon as={BiCalendar} />
            <TagText>
              {new Intl.DateTimeFormat('default', {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
              }).format(Date.parse(news.dateCreated))}
            </TagText>
          </Tag>
        )}
        {news.location && (
          <Tag>
            <TagIcon as={BiGlobe} />
            <TagText>{news.location}</TagText>
          </Tag>
        )}
        {Array.isArray(news.schemas) && news.schemas.length > 0 && (
          <Tag>
            <TagIcon as={BiPurchaseTag} />
            <TagText>
              {news.schemas.map((schema, i) => (
                <>
                  <Link
                    key={schema.id}
                    href={{ pathname: '/search', query: { schema: schema.id } }}
                  >
                    <a>{schema.label}</a>
                  </Link>
                  {i < news.schemas.length - 1 ? ', ' : ''}
                </>
              ))}
            </TagText>
          </Tag>
        )}
      </Tags>
      {Array.isArray(news.related) && (
        <>
          <RelatedList>
            {news.related.slice(0, 3).map((related) => (
              <li key={related.id}>
                <Link
                  href={`/news/${encodeURIComponent(
                    uriToId(news.id, { base: config.newsBaseUri })
                  )}?${querystring.stringify({
                    s: Buffer.from(querystring.stringify(query)).toString('base64'),
                  })}#${uriToId(related.id, { base: config.newsBaseUri })}`}
                >
                  <a>{related.headline}</a>
                </Link>
              </li>
            ))}
          </RelatedList>
          <Link
            href={`/news/${encodeURIComponent(
              uriToId(news.id, { base: config.newsBaseUri })
            )}?${querystring.stringify({
              s: Buffer.from(querystring.stringify(query)).toString('base64'),
            })}`}
          >
            <a>
              <b>
                <BiNews /> View Full Coverage ({news.related.length + 1} articles)
              </b>
            </a>
          </Link>
        </>
      )}
      <Debug>
        <pre>{JSON.stringify(news, null, 2)}</pre>
      </Debug>
    </Container>
  );
}
