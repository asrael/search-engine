import styled from 'styled-components';

import SearchInput from '@components/SearchInput';

const Container = styled.div`
  height: 48px;
  max-width: 720px;
  display: inline-block;
  padding-left: 10px;
  padding-right: 30px;
  flex: 1;

  background: #f1f3f4;
  border: 1px solid transparent;
  border-radius: 8px;
  display: flex;
  max-width: 720px;
  transition: background 100ms ease-in, width 100ms ease-out;
`;

const StyledSearchInput = styled(SearchInput)`
  .react-autosuggest__input {
    flex: 1;
    min-width: 0;
    appearance: none;
    background-color: transparent;
    border: none;
    font-size: inherit;
    outline: 0;
  }
`;

const SearchButton = styled.button`
  background: none;
  border: none;
  cursor: pointer;
  outline: none;
  line-height: 0;
  padding: 0 5px;

  svg {
    padding: 8px;
    margin: 3px;
    color: #5f6368;
    opacity: 1;
    fill: currentColor;
    width: 40px;
    height: 40px;
  }
`;

const ClearButton = styled.button`
  padding: 0 5px;
  background: none;
  border: none;
  cursor: pointer;
  outline: none;
  line-height: 0;
  position: absolute;
  right: 0;
  cursor: default;
  visibility: hidden;
  top: 0;
  transition: opacity 250ms ease-out;

  svg {
    padding: 8px;
    margin: 3px;
    color: #5f6368;
    opacity: 1;
    fill: currentColor;
    width: 40px;
    height: 40px;
  }
`;

const InputContainer = styled.div`
  height: 46px;
  margin-right: 24px;
  flex: 1;
  overflow: hidden;
  display: flex;
`;

export default function SearchBox({ placeholder, name, value, ...props }) {
  return (
    <Container {...props}>
      <SearchButton aria-label="Search" role="button">
        <svg
          focusable="false"
          height="24px"
          viewBox="0 0 24 24"
          width="24px"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M20.49,19l-5.73-5.73C15.53,12.2,16,10.91,16,9.5C16,5.91,13.09,3,9.5,3S3,5.91,3,9.5C3,13.09,5.91,16,9.5,16 c1.41,0,2.7-0.47,3.77-1.24L19,20.49L20.49,19z M5,9.5C5,7.01,7.01,5,9.5,5S14,7.01,14,9.5S11.99,14,9.5,14S5,11.99,5,9.5z"></path>
          <path d="M0,0h24v24H0V0z" fill="none"></path>
        </svg>
      </SearchButton>
      <InputContainer>
        <StyledSearchInput name={name} placeholder={placeholder} defaultValue={value} />
      </InputContainer>
      <ClearButton aria-label="Clear search" role="button">
        <svg
          focusable="false"
          height="24px"
          viewBox="0 0 24 24"
          width="24px"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path>
          <path d="M0 0h24v24H0z" fill="none"></path>
        </svg>
      </ClearButton>
    </Container>
  );
}
