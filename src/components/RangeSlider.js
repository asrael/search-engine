import { useState } from 'react';
import { Range, getTrackBackground } from 'react-range';

import theme from '~/src/theme';

export default function RangeSlider({ values = [0, 1], step = 1, min = 0, max = 1 }) {
  const [currentValues, setCurrentValues] = useState(values);

  return (
    <Range
      values={currentValues}
      step={step}
      min={min}
      max={max}
      onChange={(newValues) => setCurrentValues(newValues)}
      renderTrack={({ props, children }) => (
        <div
          role="button"
          tabIndex={0}
          style={{
            ...props.style,
            height: '36px',
            display: 'flex',
            width: '100%',
          }}
        >
          <div
            ref={props.ref}
            style={{
              height: '5px',
              width: '100%',
              borderRadius: '4px',
              background: getTrackBackground({
                values: currentValues,
                colors: ['#ccc', theme.colors.primary, '#ccc'],
                min,
                max,
              }),
              alignSelf: 'center',
            }}
          >
            {children}
          </div>
        </div>
      )}
      renderThumb={({ index, props, isDragged }) => (
        <div
          {...props}
          style={{
            ...props.style,
            outline: 0,
            height: '24px',
            width: '24px',
            borderRadius: '100%',
            backgroundColor: '#FFF',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            boxShadow: '0px 2px 6px #AAA',
          }}
        >
          <span
            style={{
              top: '-34px',
              zIndex: 1,
              position: 'absolute',
              fontSize: '0.75rem',
              transform: isDragged ? 'scale(1) translateY(-10px)' : 'scale(0)',
              transition: 'transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
              fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
              fontWeight: 400,
              lineHeight: 1.2,
              letterSpacing: '0.01071em',
              transformOrigin: 'bottom center',
            }}
          >
            <span
              style={{
                width: '32px',
                height: '32px',
                display: 'flex',
                transform: 'rotate(-45deg)',
                alignItems: 'center',
                borderRadius: '50% 50% 50% 0',
                justifyContent: 'center',
                backgroundColor: theme.colors.primary,
              }}
            >
              <span
                style={{
                  color: '#fff',
                  transform: 'rotate(45deg)',
                }}
              >
                {currentValues[index]}
              </span>
            </span>
          </span>
        </div>
      )}
    />
  );
}
