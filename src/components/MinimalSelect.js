import Select from 'react-select';
import AsyncSelect from 'react-select/async';
import theme from '~/src/theme';

export default function MinimalSelect({ async, ...props }) {
  const Component = async ? AsyncSelect : Select;
  return (
    <Component
      styles={{
        control: (provided) => ({
          ...provided,
          border: 0,
          borderLeft: '4px solid #358189',
          boxShadow: '0 0 12px -3px rgba(0,0,0,0.28)',
          '&:hover': {
            ...provided['&:hover'],
            boxShadow: '0 0 12px 0 rgba(0,0,0,0.38)',
            borderLeft: '4px solid #358189',
          },
          '&:focus': {
            ...provided['&:focus'],
            boxShadow: '0 0 12px 0 rgba(0,0,0,0.38)',
            border: '2px solid #358189',
            borderLeftWidth: '4px',
          },
        }),
      }}
      theme={(baseTheme) => ({
        ...baseTheme,
        borderRadius: 0,
        colors: {
          ...baseTheme.colors,
          primary: theme.colors.primary,
        },
      })}
      {...props}
    />
  );
}
