import styled from 'styled-components';
import { Button } from 'reakit/Button';

const StyledButton = styled(Button)`
  cursor: pointer;
  outline: 0;
  border-radius: 24px;
  box-shadow: 0 0 12px -3px rgba(0, 0, 0, 0.28);
  display: flex;
  align-items: center;
  justify-content: center;
  height: 36px;
  border: 0;
  padding: 0 24px;
  background-color: #fff;
  transition: box-shadow cubic-bezier(0.23, 1, 0.32, 1) 250ms;

  &:hover {
    box-shadow: 0 3px 12px 0px rgba(0, 0, 0, 0.28);
  }

  &:active {
    background-color: #eee;
  }
`;

export default function RoundedButton({ children, ...props }) {
  return <StyledButton {...props}>{children}</StyledButton>;
}
