import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import styled from 'styled-components';

import SearchInput from '@components/SearchInput';
import NextForm from '@components/NextForm';

const Container = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Header = styled.header`
  width: 100%;
  height: 100px;
  border-bottom: 1px solid #eaeaea;
  display: flex;
  align-items: center;

  img {
    margin-left: 3rem;
  }
`;

const Main = styled.main`
  width: 100%;
  flex: 1;
  padding: 3rem 0;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Footer = styled.footer`
  width: 100%;
  height: 100px;
  border-top: 1px solid #eaeaea;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const SearchForm = styled(NextForm)`
  display: flex;
  width: 100%;
  padding: 0 30px;
  margin-right: 86px;
`;

const StyledSearchInput = styled(SearchInput)`
  width: 720px;
  height: 48px;
  margin: 0 auto;
  flex: 1;
  padding-left: 10px;
  padding-right: 30px;
  background: #f1f3f4;
  border: 1px solid transparent;
  border-radius: 8px;
  display: flex;
  max-width: 720px;
  transition: background 100ms ease-in, width 100ms ease-out;

  .react-autosuggest__input {
    flex: 1;
    min-width: 0;
    appearance: none;
    background-color: transparent;
    border: none;
    font-size: inherit;
    outline: 0;
  }

  .react-autosuggest__container--open .react-autosuggest__suggestions-container {
    border: none;
    max-width: auto;
    min-width: 100%;
    right: auto;
    left: 0;
    top: 45px;

    background-color: #ffffff;
    outline: 0;
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.12),
      0 1px 5px 0 rgba(0, 0, 0, 0.2);
    color: #212121;
    border-bottom-left-radius: 0.25rem;
    border-bottom-right-radius: 0.25rem;
  }
`;

export default function Layout({ title, search = true, children }) {
  const { query } = useRouter();

  return (
    <Container>
      <Head>
        <title>{typeof title !== 'undefined' ? `${title} - ` : ''}ASRAEL Search Engine</title>
        <link rel="icon" href="/search-engine/favicon.png" />
      </Head>

      <Header>
        <Link href="/">
          <a>
            <img src="/search-engine/logo.png" width="86" height="50" alt="Logo" />
          </a>
        </Link>
        {search && (
          <Link href="/search" passHref>
            <SearchForm method="GET">
              <StyledSearchInput
                name="q"
                defaultValue={query.q}
                placeholder="Search for subjects, persons, locations, dates, keywords, ..."
              />
              {query.language && <input type="hidden" name="language" value={query.language} />}
            </SearchForm>
          </Link>
        )}
      </Header>

      <Main>{children}</Main>

      <Footer>
        <a href="https://asrael.eurecom.fr/" target="_blank" rel="noopener noreferrer">
          ASRAEL
        </a>
      </Footer>
    </Container>
  );
}
