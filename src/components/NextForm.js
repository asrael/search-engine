export default function NextForm({ href, children, onClick, ...props }) {
  return (
    <form action={href} {...props}>
      {children}
    </form>
  );
}
