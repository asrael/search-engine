import styled from 'styled-components';
import Link from 'next/link';
import { useRouter } from 'next/router';

import { getAnnotationColor } from '@helpers/colorize';
import { stringToColor } from '@helpers/utils';

import schemas from '../schemas.json';

const Container = styled.div`
  border: 1px solid #ddd;
  width: 280px;
`;

const Header = styled.div`
  background-color: #ddd;
  padding: 0.5em 1em;
`;

const Items = styled.div`
  padding: 1em;
`;

const Item = styled.div`
  margin-bottom: 1em;
`;

const Name = styled.span`
  display: block;
  font-weight: bold;
`;

const Value = styled.span`
  display: inline-block;
`;

const ValueDate = styled.span`
  color: #aaa;
`;

const getAnnotationName = (annotationType) => {
  if (annotationType.startsWith('http://adel.eurecom.fr/Stanford/')) {
    const annotationName = annotationType.substr('http://adel.eurecom.fr/Stanford/'.length);
    return annotationName.substr(0, 1).toUpperCase() + annotationName.substr(1).toLowerCase();
  }
  return annotationType;
};

const propertiesCache = {};
Object.keys(schemas).forEach((schemaID) => {
  const schema = schemas[schemaID];
  if (schema.properties) {
    schema.properties.forEach((property) => {
      propertiesCache[property.property_ID] = property;
    });
  }
});

const getPropName = (prop) => {
  const propID = prop.key.split('/').pop(); // extract property ID from property URL
  const property = propertiesCache[propID];
  if (property && property.property) {
    const propName = property.property
      .toLowerCase()
      .replace(/_/g, ' ')
      .split(' ')
      .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
      .join(' ');
    return propName;
  }
  return propID;
};

export default function InfoBox({ lead, documents }) {
  const { query } = useRouter();

  let sameAsList = [];
  documents.forEach((doc) => {
    if (Array.isArray(doc.sameAs)) {
      sameAsList.push(...doc.sameAs);
    }
  });
  sameAsList = Array.from(new Set(sameAsList));

  const temporalProperties = [
    'http://www.wikidata.org/prop/direct/P1120',
    'http://www.wikidata.org/prop/direct/P1339',
    'http://www.wikidata.org/prop/direct/P1446',
    'http://www.wikidata.org/prop/direct/P1590',
    'http://www.wikidata.org/prop/direct/P1561',
    'http://www.wikidata.org/prop/direct/P8010',
  ];
  const dataifications = {};
  const datas = [];
  documents.forEach((doc) => {
    if (Array.isArray(doc.data)) {
      doc.data.forEach((d) => {
        datas.push({
          dateCreated: doc.dateCreated[0],
          data: d,
        });
      });
    }
  });
  datas.sort((a, b) => new Date(a.dateCreated) - new Date(b.dateCreated));
  datas.forEach((d) => {
    if (typeof dataifications[d.data.property] === 'undefined') {
      dataifications[d.data.property] = {
        property: d.data.property,
        label: d.data.label,
        values: [],
      };
    }
    dataifications[d.data.property].values.push(d);
  });

  const entities = {};
  if (
    (typeof process !== 'undefined' && process.env.NODE_ENV === 'development') ||
    typeof query.debug !== 'undefined'
  ) {
    documents.forEach((doc) => {
      if (Array.isArray(doc.entities)) {
        doc.entities.forEach((entity) => {
          if (!Array.isArray(entities[entity.label])) {
            entities[entity.label] = [];
          }
          entities[entity.label].push(entity.text);
        });
      }
    });
  }

  return (
    <Container>
      <Header>Infobox</Header>
      <Items>
        {(lead.locality || lead.country) && (
          <Item>
            <Name>Location</Name>
            <Value>
              {lead.locality[0]} - {lead.country[0]}
            </Value>
          </Item>
        )}
        {lead.dateCreated && (
          <Item>
            <Name>Date</Name>
            <Value>
              {new Intl.DateTimeFormat('default', {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
              }).format(Date.parse(lead.dateCreated))}
            </Value>
          </Item>
        )}
        {lead.subject && lead.subjectLabel && (
          <Item>
            <Name>Subjects</Name>
            <Value>
              <ul>
                {lead.subject.map((subject, i) => (
                  <li key={subject}>
                    <Link href={{ pathname: '/search', query: { subjects: subject } }}>
                      <a>{lead.subjectLabel[i]}</a>
                    </Link>
                  </li>
                ))}
              </ul>
            </Value>
          </Item>
        )}
        {lead.genre && lead.genreLabel && (
          <Item>
            <Name>Genre</Name>
            <Value>{lead.genre[0]}</Value>
          </Item>
        )}
        {Array.isArray(sameAsList) && sameAsList.length > 0 && (
          <Item>
            <Name>Wikidata</Name>
            <Value>
              <ul>
                {sameAsList.map((sameAs) => (
                  <li key={sameAs}>
                    <a href={sameAs} target="_blank" rel="noopener noreferrer">
                      {sameAs.split('/').pop()}
                    </a>
                  </li>
                ))}
              </ul>
            </Value>
          </Item>
        )}
        {Array.isArray(lead.annotations) &&
          lead.annotations.map((annotation) => (
            <Item key={annotation.type}>
              <Name>{getAnnotationName(annotation.type)}</Name>
              <Value>
                <ul>
                  {annotation.values.map((value) => (
                    <li>
                      <a href={value.source} target="_blank" rel="noopener noreferrer">
                        <span className={`colorize color-${getAnnotationColor(annotation.type)}`}>
                          {value.label}
                        </span>
                      </a>
                    </li>
                  ))}
                </ul>
              </Value>
            </Item>
          ))}
        {/* <span *ngFor="let value of annotation.values; let last = last">
          <a [href]="value.source" target="_blank" [ngStyle]="{ 'font-weight': value.source === search.searchParams.annotations[annotation.type] ? 'bold' : 'normal' }"><span [ngClass]="['colorize', 'color-' + getAnnotationColor(annotation.type)]">{{ value.label }}</span></a><span *ngIf="!last">,</span>
        </span> */}
        {Array.isArray(lead.props) &&
          lead.props.map((prop) => (
            <Item key={prop.key}>
              <Name>{getPropName(prop)}</Name>
              <Value>{prop.value}</Value>
            </Item>
          ))}
        {Object.values(dataifications).map((d) => {
          const uniqueDisplayValues = [];
          d.values.forEach((v) => {
            if (!uniqueDisplayValues.some((u) => u.data.value === v.data.value)) {
              uniqueDisplayValues.push(v);
            }
          });
          return (
            <Item key={d.property}>
              <Name title={d.property}>{d.label}</Name>
              {uniqueDisplayValues.map((v) => (
                <div>
                  <Value style={{ color: stringToColor(v.data.value) }}>{v.data.value}</Value>
                  {temporalProperties.includes(d.property) && (
                    <>
                      {' '}
                      <ValueDate>
                        (
                        {new Intl.DateTimeFormat('default', {
                          year: 'numeric',
                          month: 'numeric',
                          day: 'numeric',
                          hour: 'numeric',
                          minute: 'numeric',
                        }).format(Date.parse(v.dateCreated))}
                        )
                      </ValueDate>
                    </>
                  )}
                </div>
              ))}
            </Item>
          );
        })}
        {Object.entries(entities).map(([key, value]) => (
          <Item key={key}>
            <Name>{key}</Name>
            <Value>
              <ul>
                {value
                  .filter((v, index, self) => {
                    return self.indexOf(v) === index;
                  })
                  .map((v) => (
                    <li>{v}</li>
                  ))}
              </ul>
            </Value>
          </Item>
        ))}
      </Items>
    </Container>
  );
}
