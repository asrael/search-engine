import { createGlobalStyle } from 'styled-components';
import { darken } from 'polished';

import theme from '~/src/theme';

export default createGlobalStyle`
  html {
    font-size: 16px;
  }

  *,
  *:before,
  *:after {
    box-sizing: inherit;
    text-rendering: geometricPrecision;
    -webkit-tap-highlight-color: transparent;
  }

  body {
    font-family: ${theme.fontFamily.sansSerif};
    background-color: ${theme.colors.background};
    color: ${theme.colors.text};

    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    text-rendering: optimizeLegibility;
    font-size: 1rem;
    line-height: 1.5;
    padding: 0;
    min-height: 100%;
    overflow-x: hidden;
    box-sizing: border-box;
  }

  a {
    color: ${theme.colors.primary};
  }

  a:hover {
    color: ${theme.colors.linkHover};
  }

  pre {
    font-family: ${theme.fontFamily.mono};
    font-size: 0.7rem;
    outline: 1px solid #ccc;
    padding: 10px;
    margin: 10px;
    overflow: auto;
    white-space: pre-wrap;
    white-space: pre-wrap;
    word-wrap: break-word;
    word-break: break-all;
    max-height: 280px;
  }

  strong, b {
    font-weight: 700;
  }

  small {
    font-size: 0.9em;
  }

  h1 {
    font-size: 3rem;
    letter-spacing: -.066875rem;
    line-height: 1.5;
    font-weight: 700;
  }

  h2 {
    font-size: 2.25rem;
    letter-spacing: -.020625rem;
  }

  h3 {
    font-size: 1.5rem;
    letter-spacing: -.029375rem;
  }

  h4 {
    font-size: 1.25rem;
    letter-spacing: -.020625rem;
  }

  h5 {
    font-size: 1rem;
    letter-spacing: -.01125rem;
  }

  h6 {
    font-size: .875rem;
    letter-spacing: -.005625rem;
  }

  h2, h3, h4, h5, h6 {
    font-weight: 700;
  }

  p {
    margin: 1rem 0;
    font-size: 1em;
    line-height: 1.625em;
    letter-spacing: -.005625rem;
    font-weight: 400;
    color: inherit;
  }

  .search-highlight {
    background: #fffc00;
    padding: 0.25em;
    box-shadow: 0px 2px 3px -1px rgba(0, 0, 0, 0.25);
    line-height: 2;
    border-radius: 2px;
  }

  .highlight {
    padding: 0.25em;
    margin: 0.25em;
    box-shadow: 0px 2px 3px -1px rgba(0, 0, 0, 0.25);
    line-height: 2;
    border-radius: 2px;
    &.primary {
      background-color: #17a2b8;
      color: #fff;
    }
    &.secondary {
      background-color: #6c757d;
      color: #fff;
    }
  }

  .underline {
    text-decoration: underline;
  }

  .colorize {
    &.color-person {
      color: #ff225a;
      &:hover {
        color: ${darken(0.2, '#ff225a')};
      }
    }
    &.color-misc {
      color: #6713e8;
      &:hover {
        color: ${darken(0.2, '#6713e8')};
      }
    }
    &.color-location {
      color: #22a6ff;
      &:hover {
        color: ${darken(0.2, '#22a6ff')};
      }
    }
    &.color-organization {
      color: #13e864;
      &:hover {
        color: ${darken(0.2, '#13e864')};
      }
    }
    &.color-thing {
      color: #D820e8;
      &:hover {
        color: ${darken(0.2, '#D820e8')};
      }
    }
    &.color-event {
      color: #ff8c22;
      &:hover {
        color: ${darken(0.2, '#ff8c22')};
      }
    }
    &.color-product {
      color: #ffcc2e;
      &:hover {
        color: ${darken(0.2, '#ffcc2e')};
      }
    }
  }

  .annotation {
    width: fit-content;
    line-height: 2em;
    text-align: center;
    display: inline-block;
    color: rgb(102, 102, 102);
    background-color: rgb(250, 250, 250);
    border-radius: 5px;
    border: 1px solid rgb(234, 234, 234);
    padding: 0px 4pt;
    font-size: 0.875rem;
    transition: box-shadow cubic-bezier(0.23, 1, 0.32, 1) 500ms;

    &:hover {
      box-shadow: 0 3px 8px -3px rgba(0, 0, 0, 0.28);
    }

    a {
      text-decoration: none;
      color: inherit;

      &:hover {
        color: ${theme.colors.text};
      }
    }
  }
`;
