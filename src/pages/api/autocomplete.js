import { withRequestValidation } from '@helpers/api';
import { getNews } from '@pages/api/search';

export default withRequestValidation({
  allowedMethods: ['POST'],
})(async (req, res) => {
  const { q } = req.body;

  const searchData = await getNews({ q });
  res.status(200).json(searchData);
});
