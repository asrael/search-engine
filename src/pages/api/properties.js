import { withRequestValidation } from '@helpers/api';
import { getPropsValues } from '@pages/api/search';

export default withRequestValidation({
  allowedMethods: ['GET'],
})(async (req, res) => {
  const { schema, id, q } = req.query;

  const results = await getPropsValues(schema, id);

  res.status(200).json(results.map((result) => ({ label: result.value, value: result.value })));
});
