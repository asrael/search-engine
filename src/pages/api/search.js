import { withRequestValidation } from '@helpers/api';
import { getPrefixes, doQuery } from '@helpers/sparql';
import { capitalize } from '@helpers/utils';
import asyncPool from 'tiny-async-pool';

import schemas from '../../schemas.json';

const groupBy = (sparqlBindings, group) =>
  Object.values(
    sparqlBindings.reduce((results, sparqlResult) => {
      const id = sparqlResult[group] ? sparqlResult[group].value : undefined;
      if (typeof id === 'undefined') {
        return [];
      }
      if (!results[id]) {
        results[id] = {};
      }
      const result = results[id];
      // eslint-disable-next-line no-restricted-syntax
      for (const key of Object.keys(sparqlResult)) {
        const value = sparqlResult[key];
        if (key === group) {
          result[group] = value.value; // ignore lang tags
        } else {
          const oldValues = result[key] || [];
          // add new value if it doesn't already exist
          if (!oldValues.includes(value.value)) {
            (result[key] || (result[key] = [])).push(value.value);
          }
        }
      }
      return results;
    }, {})
  );

export const getNews = async (context) => {
  // Subjects filter
  let subjectsFilter = '';
  if (typeof context.subjects === 'string') {
    context.subjects = [context.subjects];
  }
  if (Array.isArray(context.subjects) && context.subjects.length > 0) {
    subjectsFilter = `?id dc:subject ?subject . VALUES ?subject { ${context.subjects
      .map((uri) => `<${uri}>`)
      .join(' ')} }`;
  }

  // Genres filter
  let genresFilter = '';
  if (typeof context.genres === 'string') {
    context.genres = [context.genres];
  }
  if (Array.isArray(context.genres) && context.genres.length > 0) {
    genresFilter = `?id rnews:genre ?genre . FILTER (?genre NOT IN(${context.genres.map(
      (genre) => `<http://cv.iptc.org/newscodes/genre/${genre}>`
    )}))`;
  }

  // Date filter
  const dateFrom = context.from;
  const dateTo = context.to || dateFrom;

  const dateFilter =
    dateFrom && dateTo
      ? `FILTER(?dateCreated >= "${dateFrom}T00:00:00Z"^^xsd:dateTime && ?dateCreated <= "${dateTo}T23:59:59Z"^^xsd:dateTime)`
      : '';

  // Location filter
  const locationFilter = context.location
    ? `?id schema:contentLocation/schema:address/schema:addressCountry ?country . FILTER(CONTAINS(?country, "${context.location}"))`
    : '';

  // Keywords filter
  const keywordsFilter = context.keywords
    ? `?id rnews:articleBody ?description . ?description bif:contains '${JSON.stringify(
        context.keywords
      )}' .`
    : '';

  // Has data filter
  const hasDataFilter = context.hasData ? `?id owl:sameAs ?sameAs` : '';

  // Free text search
  let qFilter = '';
  if (context.q) {
    const words = context.q
      .split(
        /([\u0000-\u0026\u0028-\u0040\u005B-\u0060\u007B-\u00BF\u02B0-\u036F\u00D7\u00F7\u2000-\u2BFF])+/g
      )
      .filter((x) => x.trim().replace(/\W/, ''));
    const quotedBifValues = [];
    words.forEach((word) => {
      let bifValue = word.replace(/'/g, "\\'");
      if (bifValue.length >= 4) {
        bifValue += '*';
      }
      const quotedBifValue = JSON.stringify(bifValue);
      quotedBifValues.push(quotedBifValue);
    });
    qFilter += `
      {
        VALUES ?_s1p { rnews:headline schema:keywords }
        { ?id ?_s1p ?_s1o . ?_s1o bif:contains '${quotedBifValues.join(' and ')}' }
        UNION
        { ?id ?_s1p ?_s1o . FILTER(?_s1o = ${JSON.stringify(words.join(' '))}) }
      }
    `;
  }

  // Schema and properties filter
  let schemaFilter = '';
  let schemaPropertiesFilter = '';
  if (context.schema) {
    // schemaFilter += `?cluster asrael:lead ?id . ?doc schema:isPartOf ?cluster .`;

    const schema = schemas[context.schema];
    if (schema) {
      let hasProps = false;
      schema.properties.forEach((prop) => {
        if (context[prop.property_ID] && context[prop.property_ID].length > 0) {
          schemaPropertiesFilter += `?id rnews:about/wdt:${prop.property_ID} ?wdt_${
            prop.property_ID
          } . FILTER(STR(?wdt_${prop.property_ID}) = ${JSON.stringify(
            context[prop.property_ID]
          )}) `;
          hasProps = true;
        }
      });
      schemaFilter += `?id rnews:about/schema:category <http://asrael.eurecom.fr/category/${context.schema}> .`;
    }
  }

  // Language filter
  const languageFilter = context.language
    ? `?id rnews:inLanguage ${JSON.stringify(context.language)} .`
    : '';

  // Annotations filter
  let annotationsFilter = '';
  if (context.annotations) {
    annotationsFilter += `
      OPTIONAL { ?id asrael:context ?context . }
    `;
    let i = 0;
    // eslint-disable-next-line guard-for-in, no-restricted-syntax
    for (const k in context.annotations) {
      annotationsFilter += `
        {
          SELECT ?refContext_${i} WHERE {
            GRAPH <http://asrael.eurecom.fr/agencefrancepresse/news> { ?sp a nif:Phrase . }
            ?sp nif:referenceContext ?refContext_${i} .
            ?sp itsrdf:taIdentRef ?identRef_${i} .
            FILTER (?identRef_${i} = <${context.annotations[k]}>)
          }
        }
        FILTER (?context = ?refContext_${i})
      `;
      i += 1;
    }
  }

  // Has medias
  const hasMediasFilter = context.hasMedias
    ? 'FILTER EXISTS { ?id rnews:associatedMedia ?associatedMedia }'
    : '';

  // Pagination (offset/limit)
  const minPerPage = 10; // minimum number of results per page
  const maxPerPage = 50; // maximum number of results per page
  const defaultPerPage = 20; // default number of results per page
  const itemsPerPage =
    Math.max(minPerPage, Math.min(maxPerPage, parseInt(context.per_page, 10))) || defaultPerPage;

  // Sorting
  const orderByTypes = {
    newest: 'DESC(?dateCreated)',
    oldest: 'ASC(?dateCreated)',
    alphabetically: 'ASC(?headline)',
  };
  const orderBy = orderByTypes[context.sort] || orderByTypes.newest;

  const whereCondition = `
    {
      SELECT DISTINCT ?cluster ?lead ?id
      WHERE {
        ?cluster asrael:lead ?lead .
        ?lead a rnews:Article .
        ?id schema:isPartOf ?cluster .
        ?id a rnews:Article .
      }
    }
    UNION
    {
      SELECT DISTINCT ?id (?id AS ?lead)
      WHERE {
        ?id a rnews:Article .
        MINUS { ?id schema:isPartOf ?cluster }
      }
    }
    ?id a rnews:Article .
    ?id rnews:headline ?headline .
    ?id rnews:dateCreated ?dateCreated .
    OPTIONAL {
      ?id rnews:about ?event . ?event schema:category ?category .
    }
    OPTIONAL {
      ?cluster asrael:lead ?id .
      ?id schema:isPartOf ?cluster .
      ?id rnews:about ?event2 . ?event2 schema:category ?category .
    }
    ${keywordsFilter}
    ${qFilter}
    ${subjectsFilter}
    ${genresFilter}
    ${dateFilter}
    ${locationFilter}
    ${schemaFilter}
    ${schemaPropertiesFilter}
    ${languageFilter}
    ${annotationsFilter}
    ${hasMediasFilter}
    ${hasDataFilter}
  `;

  // Generate main query
  const offset = itemsPerPage * ((parseInt(context.page, 10) || 1) - 1);
  const baseQuery = `
    SELECT DISTINCT ?cluster ?lead
    WHERE {
      ${whereCondition}
    }
    ORDER BY DESC(BOUND(?cluster)) ${orderBy}
  `;
  // If offset is over 10k, Virtuoso returns an error unless we nest the select query
  const mainQuery =
    offset > 10000
      ? `
    SELECT ?cluster ?lead
    WHERE {
      {
        ${baseQuery}
      }
    }
  `
      : `
    ${baseQuery}
  `;

  const finalQuery = `
    ${getPrefixes()}
    ${mainQuery}
    OFFSET ${offset}
    LIMIT ${itemsPerPage}
  `;
  const data = await doQuery(finalQuery);

  const bindings = groupBy(data.results.bindings, 'lead');

  // Compute the total number of pages (used for pagination)
  const paginationQuery = `
    ${getPrefixes()}
    SELECT (COUNT(DISTINCT ?lead) AS ?count)
    WHERE {
      ${whereCondition}
    }
  `;
  const paginationData = await doQuery(paginationQuery);

  // Get details for each binding
  const results = [];
  const maxConcurrentRequests = 3;
  results.push(
    ...(
      await asyncPool(maxConcurrentRequests, bindings, async (binding) => {
        const detailsQuery = `
          SELECT DISTINCT *
          WHERE {
            VALUES ?id { <${binding.lead}> }
            {
              ?id rnews:headline ?headline .
            }
            UNION
            {
              ?id rnews:about ?event .
              ?event schema:category ?category .
            }
            UNION
            {
              ?id rnews:dateCreated ?dateCreated .
            }
            UNION
            {
              OPTIONAL { ?id rnews:articleBody ?description . }
            }
            UNION
            {
              OPTIONAL { ?id asrael:context ?context . }
            }
            UNION
            {
              OPTIONAL { ?id schema:contentLocation/schema:address/schema:addressCountry ?country . }
            }
            UNION
            {
              OPTIONAL { ?id schema:contentLocation/schema:address/schema:addressLocality ?locality . }
            }
          }
        `;

        const detailsData = await doQuery(detailsQuery);
        const details = groupBy(detailsData.results.bindings, 'id');

        return details;
      })
    ).flat()
  );

  const formattedResults = results.map((item) => {
    return {
      ...item,
      headline: item.headline?.[0] || '',
      location: `${item.locality}, ${item.country}`,
      schemas: (item.category || []).map((cat) => {
        const [schemaKey, schema] = Object.entries(schemas).find(([key]) => cat.endsWith(key));
        return { id: schemaKey, label: schema.label };
      }),
    };
  });

  // Get available schemas
  const schemasQuery = `
  ${getPrefixes()}
  SELECT DISTINCT ?category
  WHERE {
    ${whereCondition}
  }
  `;
  const schemasData = await doQuery(schemasQuery);
  const availableSchemas = schemasData.results.bindings.reduce((acc, binding) => {
    if (typeof binding.category === 'object') {
      const [schemaKey, schema] = Object.entries(schemas).find(([key]) =>
        binding.category.value.endsWith(key)
      );
      acc[schemaKey] = { id: schemaKey, label: schema.label };
    }
    return acc;
  }, {});

  return {
    results: formattedResults,
    availableSchemas,
    totalResults: parseInt(paginationData.results.bindings[0].count.value, 10),
    debugSparqlQuery: finalQuery,
  };
};

export const getClusterDocumentsByNews = async (newsUri) => {
  // Generate query
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT *
    WHERE {
      ?cluster a schema:CreativeWork .
      ?id a rnews:Article .
      ?id schema:isPartOf ?cluster .
      ?cluster asrael:lead <${newsUri}> .
      FILTER(?id != <${newsUri}>)
      {
        OPTIONAL { ?id rnews:headline ?headline . }
      }
      UNION
      {
        OPTIONAL { ?id rnews:identifier ?identifier . }
      }
      UNION
      {
        OPTIONAL { ?id schema:contentLocation/schema:address/schema:addressCountry ?country . }
      }
      UNION
      {
        OPTIONAL { ?id schema:contentLocation/schema:address/schema:addressLocality ?locality . }
      }
      UNION
      {
        OPTIONAL { ?id rnews:dateCreated ?dateCreated . }
      }
      UNION
      {
        OPTIONAL { ?id rnews:articleBody ?description . }
      }
      UNION
      {
        OPTIONAL { ?id rnews:articleBody ?description . }
      }
      UNION
      {
        OPTIONAL {
          ?id rnews:about ?event .
          OPTIONAL { ?event schema:category ?category . }
        }
      }
      UNION
      {
        OPTIONAL { ?id owl:sameAs ?sameAs . }
      }
      UNION
      {
        OPTIONAL { ?id asrael:context ?context . }
      }
    }
  `;

  const data = await doQuery(query);

  const bindings = groupBy(data.results.bindings, 'id');

  return bindings.map((item) => {
    return {
      ...item,
      headline: item.headline?.[0] || '',
      location: `${item.locality}, ${item.country}`,
      schemas: (item.category || []).map((cat) => {
        const [schemaKey, schema] = Object.entries(schemas).find(([key]) => cat.endsWith(key));
        return { id: schemaKey, label: schema.label };
      }),
    };
  });
};

export const getClusterHeadlinesByNews = async (newsUri) => {
  // Generate query
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT *
    WHERE {
      ?cluster a schema:CreativeWork .
      ?id a rnews:Article .
      ?id schema:isPartOf ?cluster .
      ?cluster asrael:lead <${newsUri}> .
      FILTER(?id != <${newsUri}>)
      {
        OPTIONAL { ?id rnews:headline ?headline . }
      }
      UNION
      {
        OPTIONAL { ?id rnews:dateCreated ?dateCreated . }
      }
    }
    ORDER BY ASC(?dateCreated)
  `;

  const data = await doQuery(query);

  const bindings = groupBy(data.results.bindings, 'id');

  return bindings.map((item) => {
    return {
      ...item,
      headline: item.headline?.[0] || '',
    };
  });
};

export const getNewsByUri = async (uri) => {
  // Generate query
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT ?id ?dateCreated ?dateModified ?datePublished ?description ?genre ?genreLabel ?subject ?subjectLabel ?dateline ?headline ?identifier ?language ?slug ?associatedMedia ?country ?locality ?context ?category ?sameAs ?clusterLead
    WHERE {
      ?id a rnews:Article .
      VALUES ?id { <${uri}> }
      OPTIONAL { ?id rnews:dateCreated ?dateCreated . }
      OPTIONAL { ?id rnews:dateModified ?dateModified . }
      OPTIONAL { ?id rnews:datePublished ?datePublished . }
      OPTIONAL {
        ?id rnews:articleBody ?description .
      }
      OPTIONAL {
        ?id rnews:genre ?genre .
        OPTIONAL {
          ?genre skos:prefLabel ?genreLabel .
          FILTER (LANG(?genreLabel) = "en-gb")
        }
      }
      OPTIONAL {
        ?id dc:subject ?subject .
        OPTIONAL {
          ?subject a skos:Concept .
          ?subject skos:prefLabel ?subjectLabel .
          FILTER (LANG(?subjectLabel) = "en-gb")
        }
      }
      OPTIONAL { ?id rnews:dateline ?dateline . }
      OPTIONAL { ?id rnews:headline ?headline . }
      OPTIONAL { ?id rnews:identifier ?identifier . }
      OPTIONAL { ?id rnews:inLanguage ?language . }
      OPTIONAL { ?id rnews:slug ?slug . }
      OPTIONAL { ?id rnews:associatedMedia ?associatedMedia . }
      OPTIONAL {
        ?id schema:contentLocation ?location .
        OPTIONAL {
          ?location schema:address ?address .
          OPTIONAL { ?address schema:addressCountry ?country . }
          OPTIONAL { ?address schema:addressLocality ?locality }
        }
      }
      OPTIONAL { ?id asrael:context ?context . }
      OPTIONAL {
        ?id rnews:about ?event .
        OPTIONAL { ?event schema:category ?category . }
      }
      OPTIONAL { ?id owl:sameAs ?sameAs . }
      OPTIONAL { ?id schema:isPartOf/asrael:lead ?clusterLead .}
    }
  `;

  const data = await doQuery(query);

  const bindings = groupBy(data.results.bindings, 'id');
  const news = bindings[0];

  news.category = news.category || [];

  return {
    news: {
      ...news,
      schemas:
        news.category.map((cat) => {
          const [schemaKey, schema] = Object.entries(schemas).find(([key]) => cat.endsWith(key));
          return { id: schemaKey, label: schema.label };
        }) || [],
    },
    debugSparqlQuery: query,
  };
};

export const search = async (context) => {
  // Default language: French
  if (typeof context.language === 'undefined') {
    context.language = 'fr';
  }

  const searchData = await getNews(context);

  // Populate event clusters with list of news
  for (const news of searchData.results) {
    news.related = await getClusterHeadlinesByNews(news.id);
  }

  return searchData;
};

export const getWikidataByNews = async (newsUri) => {
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT ?annotation ?property ?label ?value ?start ?end
    FROM <http://asrael.eurecom.fr/agencefrancepresse/annotations>
    WHERE {
      ?annotation <http://www.w3.org/ns/oa#hasBody> ?body .
      ?body asrael:property ?property .
      ?body rdf:value ?value .
      ?body rdfs:label ?label .
      ?annotation <http://www.w3.org/ns/oa#hasTarget> ?target .
      ?target <http://www.w3.org/ns/oa#hasSource> <${newsUri}> .
      ?target <http://www.w3.org/ns/oa#hasSelector> ?selector .
      ?selector <http://www.w3.org/ns/oa#start> ?start .
      ?selector <http://www.w3.org/ns/oa#end> ?end .
    }
  `;

  const data = await doQuery(query);

  const { bindings } = data.results;
  bindings.forEach((binding) => {
    // Normalize objects
    for (const k in binding) {
      binding[k] = binding[k].value;
    }

    binding.label = capitalize(binding.label.replace(/_/g, ' '));
  });

  return bindings;
};

export const getAnnotationsByContext = async (context) => {
  const query = `
    ${getPrefixes()}
    SELECT ?refContext ?anchorOf ?classRef ?identRef ?beginIndex ?endIndex
    FROM <http://asrael.eurecom.fr/agencefrancepresse/news>
    WHERE {
      ?sp a nif:Phrase .
      ?sp nif:referenceContext ?refContext .
      VALUES ?refContext { <${context}> }
      ?sp nif:anchorOf ?anchorOf .
      ?sp nif:beginIndex ?beginIndex .
      ?sp nif:endIndex ?endIndex .
      ?sp itsrdf:taClassRef ?classRef .
      ?sp itsrdf:taIdentRef ?identRef .
    }
  `;

  const data = await doQuery(query);

  const { bindings } = data.results;
  bindings.forEach((binding) => {
    // Normalize objects
    for (const k in binding) {
      binding[k] = binding[k].value;
    }
  });

  return bindings;
};

export const getLocations = async () => {
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT ?country
    FROM <http://asrael.eurecom.fr/agencefrancepresse/news>
    WHERE {
      ?s a rnews:Article .
      ?s schema:contentLocation ?location .
      ?location schema:address ?address .
      ?address schema:addressCountry ?country .
      ?address schema:addressLocality ?locality .
    }
    ORDER BY ASC(?country)
  `;

  const data = await doQuery(query);

  const { bindings } = data.results;
  bindings.forEach((binding) => {
    // Normalize objects
    for (const k in binding) {
      binding[k] = binding[k].value;
    }
  });

  return bindings;
};

export const getSubjectCodes = async () => {
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT ?sc ?prefLabel
    WHERE {
      {
        SELECT DISTINCT ?subject WHERE {
          GRAPH <http://asrael.eurecom.fr/agencefrancepresse/news> { ?id a rnews:Article . }
          ?id dc:subject ?subject .

          ?cluster a schema:CreativeWork .
          ?cluster asrael:lead ?id .
          ?id rnews:headline ?headline .
          FILTER (STRLEN(?headline) > 0)
          ?id rnews:about ?event .
          ?event schema:category ?category .
        }
      }
      {
        GRAPH <http://asrael.eurecom.fr/iptc/subjectcodes> { ?sc a skos:Concept . }
        ?sc skos:inScheme <http://cv.iptc.org/newscodes/subjectcode/> .
        ?sc skos:prefLabel ?prefLabel .
        FILTER (LANG(?prefLabel) = "" || LANG(?prefLabel) = "en-gb")
        FILTER (?sc = ?subject)
      }
    }
    ORDER BY ASC(?prefLabel)
  `;

  const data = await doQuery(query);

  const { bindings } = data.results;
  bindings.forEach((binding) => {
    // Normalize objects
    for (const k in binding) {
      binding[k] = binding[k].value;
    }
  });

  return bindings;
};

export const getSchemaById = async (schemaId) => {
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT ?sc ?prefLabel
    WHERE {
      GRAPH <http://asrael.eurecom.fr/categories> {
        VALUES ?sc { <http://asrael.eurecom.fr/category/${schemaId}> }
        ?sc skos:prefLabel ?prefLabel .
      }
    }
  `;

  const data = await doQuery(query);

  const { bindings } = data.results;
  bindings.forEach((binding) => {
    // Normalize objects
    for (const k in binding) {
      binding[k] = binding[k].value;
    }
  });

  return bindings[0];
};

export const getLanguages = async () => {
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT ?lang
    WHERE {
      GRAPH <http://asrael.eurecom.fr/agencefrancepresse/news> { ?s a rnews:Article . }
      ?s rnews:inLanguage ?lang .
    }
    GROUP BY ?lang
  `;

  const data = await doQuery(query);

  const { bindings } = data.results;
  bindings.forEach((binding) => {
    // Normalize objects
    for (const k in binding) {
      binding[k] = binding[k].value;
    }
  });

  return bindings;
};

export const getSources = async (newsIdentifier, baseUrl) => {
  const res = await fetch(
    `https://asrael.eurecom.fr/search-engine-assets/data/limsi/${newsIdentifier.replace(
      /:/g,
      '_'
    )}.json`
  );
  return res.ok ? res.json() : null;
};

export const getEntities = async (newsIdentifier, baseUrl) => {
  const res = await fetch(
    `https://asrael.eurecom.fr/search-engine-assets/data/entities/${newsIdentifier.replace(
      /:/g,
      '_'
    )}.json`
  );
  return res.ok ? res.json() : null;
};

export const getNewsWikiProps = async (newsUri) => {
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT *
    FROM <http://asrael.eurecom.fr/agencefrancepresse/news>
    WHERE {
      <${newsUri}> ?p ?o .
      FILTER(STRSTARTS(STR(?p), STR(wdt:)))
    }
  `;

  const data = await doQuery(query);

  const { bindings } = data.results;
  bindings.forEach((binding) => {
    // Normalize objects
    for (const k in binding) {
      binding[k] = binding[k].value;
    }
  });

  return bindings;
};

export const getPropsValues = async (schema, propId) => {
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT ?value WHERE {
      ?id rnews:about ?doc .
      ?doc schema:category <http://asrael.eurecom.fr/category/${schema}> .
      ?doc wdt:${propId} ?value .
    }
  `;

  const data = await doQuery(query);

  const { bindings } = data.results;
  bindings.forEach((binding) => {
    // Normalize objects
    for (const k in binding) {
      binding[k] = binding[k].value;
    }
  });

  return bindings;
};

export default withRequestValidation({
  allowedMethods: ['GET'],
})(async (req, res) => {
  const { query } = req;

  const searchData = await search(query);
  res.status(200).json(searchData);
});
