import { useState, useRef, useEffect, Fragment } from 'react';
import Router, { useRouter } from 'next/router';
import { useSWRInfinite } from 'swr';
import styled from 'styled-components';
import { Input } from 'reakit/Input';
import { Separator } from 'reakit/Separator';
import { countries } from 'countries-list';
import countries2to3 from 'countries-list/dist/countries2to3.json';
import countries3to2 from 'countries-list/dist/countries3to2.json';
import queryString from 'query-string';
import ReactPaginate from 'react-paginate';
import { useMenuState, Menu, MenuItem, MenuButton } from 'reakit/Menu';

import RangeSlider from '@components/RangeSlider';
import Layout from '@components/Layout';
import RoundedButton from '@components/RoundedButton';
import RoundedSelect from '@components/RoundedSelect';
import RoundedText from '@components/RoundedText';
import MinimalSelect from '@components/MinimalSelect';
import NewsCard from '@components/NewsCard';
import { search, getLocations, getSubjectCodes, getLanguages } from '@pages/api/search';
import ScrollDetector from '@components/ScrollDetector';
import { absoluteUrl, capitalize, throttle } from '@helpers/utils';
import useOnScreen from '@helpers/useOnScreen';
import useDebounce from '@helpers/useDebounce';
import Debug from '@components/Debug';
import useDidMountEffect from '@helpers/useDidMountEffect';
import Schemas from '../schemas.json';
import PropertiesTypes from '../properties_types.json';
import { addBasePath, resolveHref } from 'next/dist/next-server/lib/router/router';

const fetcher = (url) => fetch(url).then((r) => r.json());

const Container = styled.div`
  width: 100%;
  flex: 1;
  display: flex;
`;

const SearchHeader = styled.div`
  margin-bottom: 2rem;
`;

const SearchTerms = styled.div`
  color: #aaa;
  font-size: 0.9em;
`;

const SearchNavigation = styled.div`
  display: flex;
  align-items: baseline;
  flex-wrap: wrap;
`;

const NavItems = styled.div`
  display: flex;
  align-items: center;
  height: 54px;
  margin-left: auto;
`;

const NavItem = styled.div`
  flex: 1;

  &:not(:last-child) {
    margin-right: 24px;
  }
`;

const Content = styled.div`
  flex: 1;
`;

const FiltersSidebar = styled.aside`
  margin: 0 5rem;
  max-width: 300px;

  h3 {
    margin-bottom: 1rem;
  }

  section {
    margin-bottom: 1rem;
    display: flex;
    flex-direction: column;

    h4 {
      margin-bottom: 1rem;
      text-transform: capitalize;
    }
  }
`;

const NarrowSidebar = styled.aside`
  margin: 0 5rem;

  > div {
    background-color: #f8f9fa;
    padding: 2rem;
  }

  h3 {
    margin-bottom: 1rem;
  }

  section {
    margin-bottom: 2rem;
    display: flex;
    flex-direction: column;

    h4 {
      margin-bottom: 1rem;
    }
  }
`;

const MinimalInput = styled(Input)`
  box-sizing: border-box;
  min-height: 38px;
  padding: 2px 8px;
  outline: 0;
  border: 0;
  font: inherit;
  color: inherit;
  border-left: 4px solid ${({ theme }) => theme.colors.primary};
  box-shadow: 0 0 12px -3px rgba(0, 0, 0, 0.28);
  transition: box-shadow cubic-bezier(0.23, 1, 0.32, 1) 250ms;

  &:hover,
  &:focus {
    box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.38);
  }

  &:focus {
    border: 2px solid ${({ theme }) => theme.colors.primary};
    border-left-width: 4px;
  }
`;

const StyledSeparator = styled(Separator)`
  border: 1px solid #eee;
  margin: 2rem 0;
`;

const Results = styled.div`
  transition: opacity 250ms cubic-bezier(0.23, 1, 0.32, 1) 0s;
  opacity: ${({ loading }) => (loading ? 0.25 : 1)};
  pointer-events: ${({ loading }) => (loading ? 'none' : 'auto')};
`;

const ResultPage = styled.h3`
  margin-top: 2rem;
  margin-bottom: 1rem;
`;

const PaginationContainer = styled.div`
  position: sticky;
  bottom: 0;
  z-index: 100000;
  background-color: ${({ theme }) => theme.colors.background};
  padding-bottom: 20px;
  padding-top: 20px;
  li {
    display: inline-block;
    margin-left: -1px;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    user-select: none;
    border: 1px solid #e1e4e8;
    transition: background-color 250ms cubic-bezier(0.23, 1, 0.32, 1) 0s;
    a {
      display: inline-block;
      padding: 7px 12px;
      transition: color 250ms cubic-bezier(0.23, 1, 0.32, 1) 0s;
      color: ${({ theme }) => theme.colors.primary};
      &:hover {
        font-weight: 700;
      }
    }
    &.break {
    }
    &.active {
      background-color: ${({ theme }) => theme.colors.primary};
      a {
        color: #fff;
      }
    }
  }
`;

const StyledMenu = styled(Menu)`
  max-height: 300px;
  overflow-y: auto;
  padding-bottom: 4px;
  padding-top: 4px;
  position: relative;
  -webkit-overflow-scrolling: touch;
  box-sizing: border-box;
  outline: 0;
  background-color: hsl(0, 0%, 100%);
  border-radius: 4px;
  box-shadow: 0 0 0 1px hsla(0, 0%, 0%, 0.1), 0 4px 11px hsla(0, 0%, 0%, 0.1);
  margin-bottom: 8px;
  margin-top: 8px;
`;

const StyledMenuItem = styled(MenuItem)`
  background-color: transparent;
  color: inherit;
  cursor: default;
  display: block;
  font-size: inherit;
  padding: 8px 12px;
  width: 100%;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  box-sizing: border-box;
  border: 0;
  border-radius: 0;
  outline: 0;

  &:hover {
    background-color: #deebff;
    color: inherit;
    cursor: default;
    display: block;
    font-size: inherit;
    padding: 8px 12px;
    width: 100%;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    box-sizing: border-box;
  }

  &:active {
    background-color: #b2d4ff;
  }

  &.selected {
    background-color: #2684ff;
    color: hsl(0, 0%, 100%);
    cursor: default;
    display: block;
    font-size: inherit;
    padding: 8px 12px;
    width: 100%;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    box-sizing: border-box;
  }
`;

const countriesOptions = Object.entries(countries).map(([countryKey, country]) => ({
  label: `${country.emoji} ${country.name}`,
  value: countries2to3[countryKey],
}));
countriesOptions.sort((a, b) => a.label.localeCompare(b.label));

const sortOptions = [
  { label: 'Newest events first', value: 'newest' },
  { label: 'Oldest events first', value: 'oldest' },
  { label: 'Alphabetically', value: 'alphabetically' },
];

const languagesLabels = { fr: 'French', en: 'English' };
const languageEmojis = { fr: '🇫🇷', en: '🇺🇸' };

export default function SearchPage({
  initialData,
  locationsOptions,
  subjectCodesOptions,
  languagesOptions,
}) {
  const { req, query, pathname } = useRouter();
  const currentPage = parseInt(query.page, 10) || 1;

  const initialFields = {};

  const [isPageLoading, setIsPageLoading] = useState(false);
  const [fields, setFields] = useState(initialFields);

  useEffect(() => {
    const newFields = {};
    Object.entries(query).forEach(([key, value]) => {
      newFields[key] = value;
    });
    setFields(newFields);
  }, [query]);

  // Store the initial start page on load, because `currentPage`
  // gets updated during infinite scroll.
  const [initialPage, setInitialPage] = useState(currentPage);

  // A function to get the SWR key of each page,
  // its return value will be accepted by `fetcher`.
  // If `null` is returned, the request of that page won't start.
  const getKey = (pageIndex, previousPageData) => {
    if (previousPageData && !previousPageData.results.length) return null; // reached the end
    const q = { ...query, page: initialPage + pageIndex };
    return `${absoluteUrl(req)}${addBasePath(
      resolveHref(pathname, '/api/search')
    )}?${queryString.stringify(q)}`; // SWR key
  };

  const PAGE_SIZE = 20;
  const { data = [initialData], error, size, setSize } = useSWRInfinite(getKey, fetcher, {
    persistSize: true,
  });

  const isLoadingInitialData = !data && !error;
  const isLoadingMore = isLoadingInitialData || (data && typeof data[size - 1] === 'undefined');
  const isReachingEnd = data && data[data.length - 1]?.results.length < PAGE_SIZE;
  const isEmpty = data?.[0]?.results.length === 0;

  let totalPages = 0;
  let totalResults = 0;
  let debugSparqlQuery = null;
  let availableSchemas = [];
  if (data && data[0]) {
    totalResults = data[0].totalResults;
    debugSparqlQuery = data[0].debugSparqlQuery;
    availableSchemas = data[0].availableSchemas;
  }
  totalPages = Math.ceil(totalResults / PAGE_SIZE);

  const schemasOptions = Object.entries(availableSchemas || Schemas).map(([schemaKey, schema]) => ({
    label: capitalize(schema.label),
    value: schemaKey,
  }));
  schemasOptions.sort((a, b) => a.label.localeCompare(b.label));

  const loadMore = () => {
    if (isLoadingMore || currentPage + 1 > totalPages) return;

    setSize(size + 1);
  };

  const $loadMoreTrigger = useRef(null);
  const isOnScreen = useOnScreen($loadMoreTrigger);

  useEffect(() => {
    if (isOnScreen) loadMore();
  }, [isOnScreen]);

  const loadPage = (pageNumber) => {
    setSize(1);
    setInitialPage(pageNumber);
    return Router.push(
      {
        pathname,
        query: {
          ...query,
          page: pageNumber,
        },
      },
      undefined,
      { shallow: true }
    );
  };

  const onPageChange = (pageItem) => {
    const pageIndex = parseInt(pageItem.selected, 10);
    if (Number.isNaN(pageIndex)) {
      return;
    }

    const pageNumber = pageIndex + 1;
    if (pageNumber === currentPage) {
      return;
    }

    loadPage(pageNumber).then(() => window.scrollTo(0, 0));
  };

  const onScrollToPage = (pageIndex) => {
    if (initialPage + pageIndex !== query.page) {
      Router.push(
        {
          pathname,
          query: {
            ...query,
            page: initialPage + pageIndex,
          },
        },
        undefined,
        { shallow: true }
      );
    }
  };

  const onChangeProperty = (event, meta) => {
    let fieldName;
    let fieldValue;
    if (meta) {
      // react-select doesn't return the same parameters as other inputs
      // the first parameter "event" contains the selected label/value
      // the second parameter "meta" contains the input name
      fieldName = meta.name;
      fieldValue = event;
    } else {
      const { target } = event;
      fieldName = target.name;
      if (target.type === 'checkbox') {
        fieldValue = target.checked ? 1 : 0;
      } else {
        fieldValue = target.value;
      }
    }
    console.log(fieldName, '=', fieldValue);
    setFields((prev) => ({
      ...prev,
      [fieldName]: fieldValue,
    }));
  };

  const doSearch = () => {
    console.log('doSearch', fields);

    const searchFields = {};
    Object.entries(fields).forEach(([key, val]) => {
      if (typeof val === 'undefined' || val === null || val === '') {
        delete searchFields[key];
      } else if (typeof val === 'boolean') {
        searchFields[key] = val ? 1 : 0;
      } else if (val.value) {
        searchFields[key] = val.value;
      } else if (Array.isArray(val)) {
        searchFields[key] = val.map((v) => v.value || v);
      } else {
        searchFields[key] = val;
      }
    });

    const newQuery = {
      ...searchFields,
    };

    // If fields are the same, ignore search
    if (JSON.stringify(query) === JSON.stringify(newQuery)) {
      return;
    }

    // If schema has changed, remove other properties
    if (newQuery.schema && newQuery.schema !== query.schema) {
      Object.keys(newQuery)
        .filter((key) => key.startsWith('P'))
        .forEach((key) => {
          if (!Schemas[newQuery.schema].properties.some((prop) => prop.property_ID === key)) {
            delete newQuery[key];
          }
        });
    }

    // Reset page index
    setSize(1);
    setInitialPage(1);
    delete newQuery.page;

    Router.push(
      {
        pathname,
        query: newQuery,
      },
      undefined
    );
  };

  const debouncedFields = useDebounce(fields, 500);
  useDidMountEffect(() => {
    doSearch();
  }, [debouncedFields]);

  Router.events.on('routeChangeStart', () => setIsPageLoading(true));
  Router.events.on('routeChangeComplete', () => setIsPageLoading(false));
  Router.events.on('routeChangeError', () => setIsPageLoading(false));

  const renderEmptyResults = () => {
    return <p>No results</p>;
  };

  const renderPropertyInput = (prop) => {
    // if (PropertiesTypes[prop.property_ID].propertyType === 'http://wikiba.se/ontology#Quantity') {
    //   return (
    //     <MinimalInput
    //       placeholder="Enter a number"
    //       name={prop.property_ID}
    //       type="number"
    //       onChange={onChangeProperty}
    //       defaultValue={fields[prop.property_ID]}
    //     />
    //   );
    // }
    return (
      <MinimalSelect
        name={prop.property_ID}
        async
        cacheOptions
        defaultOptions
        loadOptions={async (inputValue) => {
          return (
            await fetch(
              `${absoluteUrl(req)}${addBasePath(
                resolveHref(pathname, '/api/properties')
              )}?schema=${encodeURIComponent(fields.schema)}&id=${encodeURIComponent(
                prop.property_ID
              )}&q=${encodeURIComponent(inputValue)}`
            )
          ).json();
        }}
        onChange={onChangeProperty}
        defaultInputValue={fields[prop.property_ID]}
        isClearable
      />
    );
  };

  const languageMenu = useMenuState();

  return (
    <Layout>
      <Container>
        <FiltersSidebar>
          <section>
            <h3>Select an Event Schema</h3>
            <MinimalSelect
              name="schema"
              options={schemasOptions}
              onChange={onChangeProperty}
              value={schemasOptions.find((option) => option.value === fields.schema)}
              isClearable
            />
          </section>
          {fields.schema && (
            <>
              <StyledSeparator />
              {fields.schema in Schemas &&
                Schemas[fields.schema].properties
                  .filter(
                    (prop) => !['instance_of', 'subclass_of', 'part_of'].includes(prop.property)
                  )
                  .map((prop) => (
                    <section key={prop.property_ID}>
                      <h4>{prop.property.replace(/_/g, ' ')}</h4>
                      {renderPropertyInput(prop)}
                    </section>
                  ))}
            </>
          )}
          {/* <section>
            <h4>Number of deaths</h4>
            <RangeSlider min={0} max={20} values={[0, 20]} />
          </section>
          <section>
            <h4>Country</h4>
            <MinimalSelect options={countriesOptions} isClearable />
          </section>
          <section>
            <h4>Item operated</h4>
            <MinimalInput type="text" placeholder="Enter a value" />
          </section> */}
        </FiltersSidebar>

        <Content>
          <Debug>
            <pre>{debugSparqlQuery}</pre>
          </Debug>

          <SearchHeader>
            <SearchNavigation>
              <h3>{isPageLoading ? 'Loading...' : `Found ${totalResults} events`}</h3>
              <NavItems>
                <NavItem>
                  <StyledMenu {...languageMenu} tabIndex={0} aria-label="Preferences">
                    {languagesOptions.map((opt) => {
                      return (
                        <StyledMenuItem
                          {...languageMenu}
                          key={opt.value}
                          className={opt.value === query.language ? 'selected' : ''}
                          onClick={() => {
                            onChangeProperty(opt.value, { name: 'language' });
                            languageMenu.hide();
                          }}
                        >
                          {languageEmojis[opt.value]} {opt.label}
                        </StyledMenuItem>
                      );
                    })}
                  </StyledMenu>
                  <RoundedButton
                    as={MenuButton}
                    {...languageMenu}
                    name="language"
                    options={languagesOptions}
                    style={{ padding: '0 12px' }}
                  >
                    {languageEmojis[query.language || 'fr']}
                  </RoundedButton>
                </NavItem>
                <NavItem>
                  <RoundedSelect
                    name="sort"
                    label="Sort by"
                    options={sortOptions}
                    defaultValue={sortOptions.find((option) => option.value === 'newest')}
                    isSearchable={false}
                    onChange={onChangeProperty}
                  />
                </NavItem>
                {/* <NavItem>
                  <RoundedButton size="small" auto>Share</RoundedButton>
                </NavItem> */}
              </NavItems>
            </SearchNavigation>
            {query.q && (
              <SearchTerms>
                Search terms:{' '}
                <RoundedText>
                  <b>{query.q}</b>
                </RoundedText>
              </SearchTerms>
            )}
          </SearchHeader>

          {isEmpty ? (
            renderEmptyResults()
          ) : (
            <>
              {data.map((page, i) => {
                const pageIndex = i;
                return (
                  <Fragment key={pageIndex}>
                    {page.results.length > 0 && (
                      <ResultPage>
                        {initialPage + pageIndex > 1 && <>{`Page ${initialPage + pageIndex}`}</>}
                      </ResultPage>
                    )}
                    <ScrollDetector
                      onAppears={() => onScrollToPage(pageIndex)}
                      rootMargin="0px 0px -50% 0px"
                    />
                    <Results loading={isPageLoading || isLoadingInitialData ? 1 : 0}>
                      {page.results.map((news) => (
                        <NewsCard key={news.id} news={news} />
                      ))}
                    </Results>
                  </Fragment>
                );
              })}
              <div style={{ margin: '24px 0' }} ref={$loadMoreTrigger}>
                <RoundedButton
                  loading={isLoadingMore}
                  disabled={isReachingEnd}
                  onClick={() => {
                    loadMore();
                  }}
                >
                  {isLoadingMore ? 'Loading...' : 'Load more'}
                </RoundedButton>
              </div>
              <PaginationContainer>
                <ReactPaginate
                  previousLabel="Previous"
                  previousAriaLabel="Previous"
                  nextLabel="Next"
                  nextAriaLabel="Next"
                  breakLabel="..."
                  breakClassName="break"
                  pageCount={totalPages}
                  initialPage={initialPage - 1}
                  forcePage={currentPage - 1}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={5}
                  onPageChange={onPageChange}
                  disableInitialCallback
                  containerClassName="pagination"
                  subContainerClassName="pages pagination"
                  activeClassName="active"
                />
              </PaginationContainer>
            </>
          )}
        </Content>

        <NarrowSidebar>
          <div>
            <h3>Narrow your search</h3>
            <section>
              <h4>What? (subject code)</h4>
              <MinimalSelect
                name="subjects"
                value={subjectCodesOptions.find((option) => {
                  return Array.isArray(fields.subjects)
                    ? fields.subjects.includes(option.value)
                    : fields.subjects === option.value;
                })}
                options={subjectCodesOptions}
                onChange={onChangeProperty}
                isClearable
              />
            </section>
            <section>
              <h4>Where? (news created)</h4>
              <MinimalSelect
                name="location"
                options={locationsOptions}
                onChange={onChangeProperty}
                isClearable
              />
            </section>
            <section>
              <h4>When? (news created)</h4>
              <div style={{ marginBottom: '1em' }}>
                <div>
                  <small>
                    <b>From</b>
                  </small>
                </div>
                <MinimalInput name="from" type="date" onChange={onChangeProperty} />
              </div>
              <div>
                <div>
                  <small>
                    <b>To</b>
                  </small>
                </div>
                <MinimalInput name="to" type="date" onChange={onChangeProperty} />
              </div>
            </section>
            <section>
              <h4>
                <label>
                  Wikidata only <input name="hasData" onChange={onChangeProperty} type="checkbox" />
                </label>
              </h4>
            </section>
          </div>
        </NarrowSidebar>
      </Container>
    </Layout>
  );
}

export async function getServerSideProps({ query }) {
  const searchData = await search(query);

  const subjectsCodes = await getSubjectCodes();
  const subjectCodesOptions = subjectsCodes.map((subj) => ({
    label: capitalize(subj.prefLabel),
    value: subj.sc,
  }));
  subjectCodesOptions.sort((a, b) => a.label.localeCompare(b.label));

  const locations = await getLocations();
  const locationsOptions = locations
    .map((loc) => {
      const country = countries[countries3to2[loc.country]];
      if (country) {
        return {
          label: `${country.emoji} ${country.name}`,
          value: loc.country,
        };
      }
      return null;
    })
    .filter((x) => x);
  locationsOptions.sort((a, b) => a.label.localeCompare(b.label));

  const languages = await getLanguages();
  const languagesOptions = languages.map((lang) => ({
    label: languagesLabels[lang.lang] || lang.lang,
    value: lang.lang,
  }));
  locationsOptions.sort((a, b) => a.label.localeCompare(b.label));

  return {
    props: {
      initialData: searchData,
      locationsOptions,
      subjectCodesOptions,
      languagesOptions,
    },
  };
}
