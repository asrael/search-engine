import styled from 'styled-components';
import { Separator } from 'reakit/Separator';

import Layout from '@components/Layout';
import { doQuery, getPrefixes } from '@helpers/sparql';

const Container = styled.div`
  width: 100%;
  flex: 1;
  max-width: 960px;
  margin: 0 auto;

  ul > li {
    margin-left: 1em;
  }
`;

const QueryLink = styled.span`
  font-size: 0.7em;
  font-style: italic;
`;

const StyledSeparator = styled(Separator)`
  border: 1px solid #eee;
  margin: 2rem 0;
`;

const ScrollContainer = styled.div`
  outline: 1px solid #ccc;
  padding: 10px;
  margin: 10px;
  overflow: auto;
  white-space: pre-wrap;
  white-space: pre-wrap;
  word-wrap: break-word;
  word-break: break-all;
  max-height: 280px;
`;

const generateQueryLink = (query) => {
  return `https://asrael.eurecom.fr/sparql?qtxt=${encodeURIComponent(query)}`;
};

export default function StatsPage({
  locations,
  subjectsCodes,
  clustersCount,
  newsCount,
  mostRecentNews,
  oldestNews,
  matchingCount,
}) {
  return (
    <Layout>
      <Container>
        <h1>Statistics of the ASRAEL KG</h1>
        <StyledSeparator />

        <h2>News Items</h2>
        <ul>
          <li>
            Total:{' '}
            {newsCount.results.reduce((acc, cur) => {
              acc += cur.count;
              return acc;
            }, 0)}{' '}
            news{' '}
            <QueryLink>
              (
              <a href={generateQueryLink(newsCount.query)} target="_blank" rel="noreferrer">
                query
              </a>
              )
            </QueryLink>
          </li>
          <li>
            <ul>
              {newsCount.results.map((cur) => {
                return (
                  <li>
                    {cur.year} ({cur.language}): {cur.count} news
                  </li>
                );
              })}
            </ul>
          </li>
          <li>
            Most recent:{' '}
            <a href={mostRecentNews.results.uri} rel="noreferrer" target="_blank">
              {mostRecentNews.results.headline}
            </a>{' '}
            ({mostRecentNews.results.date}){' '}
            <QueryLink>
              (
              <a href={generateQueryLink(mostRecentNews.query)} target="_blank" rel="noreferrer">
                query
              </a>
              )
            </QueryLink>
          </li>
          <li>
            Oldest:{' '}
            <a href={oldestNews.results.uri} rel="noreferrer" target="_blank">
              {oldestNews.results.headline}
            </a>{' '}
            ({oldestNews.results.date}){' '}
            <QueryLink>
              (
              <a href={generateQueryLink(oldestNews.query)} target="_blank" rel="noreferrer">
                query
              </a>
              )
            </QueryLink>
          </li>
        </ul>
        <br />

        <h2>Clustering (nb of events)</h2>
        <ul>
          <li>
            Total:{' '}
            {clustersCount.results.reduce((acc, cur) => {
              acc += cur.count;
              return acc;
            }, 0)}{' '}
            clusters{' '}
            <QueryLink>
              (
              <a href={generateQueryLink(clustersCount.query)} target="_blank" rel="noreferrer">
                query
              </a>
              )
            </QueryLink>
          </li>
          <li>
            <ul>
              {clustersCount.results.map((cur) => {
                return (
                  <li>
                    {cur.year} ({cur.language}): {cur.count} clusters
                  </li>
                );
              })}
            </ul>
          </li>
        </ul>
        <br />

        <h2>Nb of news items aligned with Wikidata</h2>
        <ul>
          <li>
            Total:{' '}
            {matchingCount.results.reduce((acc, cur) => {
              acc += cur.count;
              return acc;
            }, 0)}{' '}
            news items matched with Wikidata items{' '}
            <QueryLink>
              (
              <a href={generateQueryLink(matchingCount.query)} target="_blank" rel="noreferrer">
                query
              </a>
              )
            </QueryLink>
          </li>
          <li>
            <ul>
              {matchingCount.results.map((cur) => {
                return (
                  <li>
                    {cur.year} ({cur.language}): {cur.count} matches
                  </li>
                );
              })}
            </ul>
          </li>
        </ul>
        <br />

        <h2>Other stats</h2>
        <div>
          {Object.keys(subjectsCodes).length} subjects codes being used, top 10 (by frequency) is:
          <ul>
            {subjectsCodes.slice(0, 10).map((subj) => (
              <li>
                ({subj.nbDoc}){' '}
                <a href={subj.sc} rel="noreferrer" target="_blank">
                  {subj.sc} ({subj.prefLabel})
                </a>
              </li>
            ))}
          </ul>
        </div>
        <br />
        <div>
          {Object.keys(locations).length} countries being mentioned, top 10 (by frequency) is:
          <ul>
            {locations.slice(0, 10).map((loc) => (
              <li>
                ({loc.nbDoc}) {loc.country}
              </li>
            ))}
          </ul>
        </div>
        <br />
      </Container>
    </Layout>
  );
}

const getClustersCount = async () => {
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT ?language ?year (COUNT(DISTINCT ?cluster) AS ?count) WHERE {
      ?cluster a schema:CreativeWork .
      ?cluster asrael:lead ?lead .
      ?lead rnews:inLanguage ?language .
      ?lead rnews:dateCreated ?dateCreated .
      BIND(YEAR(?dateCreated) AS ?year)
    }
    GROUP BY ?language ?year
    ORDER BY ASC(?year) ASC(?language)
  `;
  const data = await doQuery(query);
  return {
    query,
    results: data.results.bindings.map((bind) => ({
      language: bind.language.value,
      year: bind.year.value,
      count: parseInt(bind.count.value, 10),
    })),
  };
};

const getNewsCount = async () => {
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT ?language ?year (COUNT(DISTINCT ?news) AS ?count) WHERE {
      ?news a rnews:Article .
      ?news rnews:inLanguage ?language .
      ?news rnews:dateCreated ?dateCreated .
      BIND(YEAR(?dateCreated) AS ?year)
    }
    GROUP BY ?language ?year
    ORDER BY ASC(?year) ASC(?language)
  `;
  const data = await doQuery(query);
  return {
    query,
    results: data.results.bindings.map((bind) => ({
      language: bind.language.value,
      year: bind.year.value,
      count: parseInt(bind.count.value, 10),
    })),
  };
};

const getMostRecentNews = async () => {
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT ?news ?headline ?dateCreated WHERE {
      ?news a rnews:Article .
      ?news rnews:headline ?headline .
      ?news rnews:dateCreated ?dateCreated .
    }
    ORDER BY DESC(?dateCreated)
    LIMIT 1
  `;
  const data = await doQuery(query);
  const bind = data.results.bindings[0];
  return {
    query,
    results: {
      uri: bind.news.value,
      headline: bind.headline.value,
      date: bind.dateCreated.value,
    },
  };
};

const getOldestNews = async () => {
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT ?news ?headline ?dateCreated WHERE {
      ?news a rnews:Article .
      ?news rnews:headline ?headline .
      ?news rnews:dateCreated ?dateCreated .
    }
    ORDER BY ASC(?dateCreated)
    LIMIT 1
  `;
  const data = await doQuery(query);
  const bind = data.results.bindings[0];
  return {
    query,
    results: {
      uri: bind.news.value,
      headline: bind.headline.value,
      date: bind.dateCreated.value,
    },
  };
};

const getMatchingCount = async () => {
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT ?language ?year (COUNT(DISTINCT ?news) AS ?count) WHERE {
      ?news a rnews:Article .
      ?news owl:sameAs ?sameAs .
      ?news rnews:inLanguage ?language .
      ?news rnews:dateCreated ?dateCreated .
      BIND(YEAR(?dateCreated) AS ?year)
    }
    GROUP BY ?language ?year
    ORDER BY ASC(?year) ASC(?language)
  `;
  const data = await doQuery(query);
  return {
    query,
    results: data.results.bindings.map((bind) => ({
      language: bind.language.value,
      year: bind.year.value,
      count: parseInt(bind.count.value, 10),
    })),
  };
};

const getTopLocations = async () => {
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT ?country (COUNT(DISTINCT ?s) AS ?nbDoc)
    FROM <http://asrael.eurecom.fr/agencefrancepresse/news>
    WHERE {
      ?s a rnews:Article .
      ?s schema:contentLocation ?location .
      ?location schema:address ?address .
      ?address schema:addressCountry ?country .
      ?address schema:addressLocality ?locality .
    }
    ORDER BY DESC(?nbDoc)
  `;

  const data = await doQuery(query);

  const { bindings } = data.results;
  bindings.forEach((binding) => {
    // Normalize objects
    for (const k in binding) {
      binding[k] = binding[k].value;
    }
  });

  return bindings;
};

const getTopSubjectCodes = async () => {
  const query = `
    ${getPrefixes()}
    SELECT DISTINCT ?sc ?prefLabel ?nbDoc
    WHERE {
      {
        SELECT DISTINCT ?subject (COUNT(DISTINCT ?id) AS ?nbDoc) WHERE {
          GRAPH <http://asrael.eurecom.fr/agencefrancepresse/news> { ?id a rnews:Article . }
          ?id dc:subject ?subject .

          ?cluster a schema:CreativeWork .
          ?cluster asrael:lead ?id .
          ?id rnews:headline ?headline .
          FILTER (STRLEN(?headline) > 0)
          ?id rnews:about ?event .
          ?event schema:category ?category .
        }
      }
      {
        GRAPH <http://asrael.eurecom.fr/iptc/subjectcodes> { ?sc a skos:Concept . }
        ?sc skos:inScheme <http://cv.iptc.org/newscodes/subjectcode/> .
        ?sc skos:prefLabel ?prefLabel .
        FILTER (LANG(?prefLabel) = "" || LANG(?prefLabel) = "en-gb")
        FILTER (?sc = ?subject)
      }
    }
    ORDER BY DESC(?nbDoc)
  `;

  const data = await doQuery(query);

  const { bindings } = data.results;
  bindings.forEach((binding) => {
    // Normalize objects
    for (const k in binding) {
      binding[k] = binding[k].value;
    }
  });

  return bindings;
};

export async function getServerSideProps() {
  const subjectsCodes = await getTopSubjectCodes();
  const locations = await getTopLocations();

  const clustersCount = await getClustersCount();
  const newsCount = await getNewsCount();
  const mostRecentNews = await getMostRecentNews();
  const oldestNews = await getOldestNews();
  const matchingCount = await getMatchingCount();

  return {
    props: {
      locations,
      subjectsCodes,
      clustersCount,
      newsCount,
      mostRecentNews,
      oldestNews,
      matchingCount,
    },
  };
}
