import { Fragment } from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import { BiCalendar, BiGlobe, BiPurchaseTag, BiLink, BiNews } from 'react-icons/bi';
import querystring from 'querystring';
import StickyBox from 'react-sticky-box';

import {
  getNewsByUri,
  getClusterDocumentsByNews,
  getAnnotationsByContext,
  getWikidataByNews,
  getSources,
  getEntities,
  getNewsWikiProps,
  getSubjectCodes,
  getSchemaById,
} from '@pages/api/search';
import Layout from '@components/Layout';
import InfoBox from '@components/InfoBox';
import Debug from '@components/Debug';
import annotate from '@helpers/annotate';
import underline from '@helpers/underline';
import RoundedButton from '@components/RoundedButton';
import { absoluteUrl, capitalize, uriToId, idToUri } from '@helpers/utils';
import config from '~/src/config';
import theme from '~/src/theme';

const Container = styled.div`
  width: 80%;
  max-width: 1200px;
  flex: 1;
  display: flex;
  align-items: flex-start;
`;

const Content = styled.div`
  flex: 1;
  margin-right: 4em;
  h1 {
    margin-bottom: 1rem;
  }
`;

const Tags = styled.div`
  display: flex;
  margin: 0.25rem 0;
`;

const Tag = styled.div`
  display: flex;
  align-items: center;
  margin-right: 1rem;
`;

const TagIcon = styled.svg`
  margin-right: 0.5rem;
  flex-shrink: 0;
`;

const TagText = styled.span`
  color: #aaa;
`;

const Description = styled.div``;

const Breadcrumbs = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 2em;
`;

const Breadcrumb = styled.span`
  border-radius: 24px;
  box-shadow: 0 0 12px -3px rgba(0, 0, 0, 0.28);
  display: flex;
  align-items: center;
  height: 36px;
  border: 0;
  padding: 0 24px;
  background-color: #fff;

  &:not(:last-child) {
    margin-right: 1em;
  }

  &:hover {
    box-shadow: 0 3px 12px 0px rgba(0, 0, 0, 0.28);
  }

  &:active {
    background-color: #eee;
  }

  a {
    text-decoration: none;
  }
`;

const Anchor = styled.a`
  visibility: hidden;
  position: absolute;
  top: 0.375em;
  right: 0;
  text-decoration: none;
  font-size: 0.75em;
  color: #ccc;

  &:hover {
    color: #999;
  }
`;

const Card = styled.div`
  border: 1px solid #ddd;
  border-radius: 8px;
  padding: 1em 2em;

  h1 {
    position: relative;
    padding-right: 0.75em;
    word-break: break-word;
  }

  &:hover ${Anchor} {
    visibility: visible;
  }
`;

const Bubble = styled.div`
  background-color: #ddd;
  width: 3em;
  height: 3em;
  margin: 2em auto;
  border-radius: 100%;
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  font-size: 0.9em;
  line-height: 1.1em;
  padding: 2em;

  &:before,
  &:after {
    content: '';
    background-color: #ddd;
    font-weight: bold;
    display: block;
    height: 2em;
    width: 2px;
    position: absolute;
    margin: 0 auto;
  }

  &:before {
    width: 2px;
    top: -2em;
  }

  &:after {
    bottom: -2em;
  }
`;

const breadcrumbsTags = {
  q: 'Search terms',
  sort: null,
  page: null,
  hasData: null,
};

export default function DetailsPage({ documents, lead, breadcrumbs, debugSparqlQuery }) {
  return (
    <Layout title={lead.headline}>
      <Container>
        <Content>
          {Object.keys(breadcrumbs).length > 0 && (
            <Breadcrumbs>
              {Object.entries(breadcrumbs)
                .filter(
                  ([key, value]) =>
                    typeof key !== 'undefined' &&
                    typeof value !== 'undefined' &&
                    value !== null &&
                    breadcrumbsTags[key] !== null
                )
                .map(([key, value]) => (
                  <Breadcrumb key={key}>
                    <Link href={{ pathname: '/search', query: breadcrumbs }}>
                      <a>
                        {`${breadcrumbsTags[key] || capitalize(key)}`}: <b>{value}</b>
                      </a>
                    </Link>
                  </Breadcrumb>
                ))}
            </Breadcrumbs>
          )}

          <Debug>
            <pre>{debugSparqlQuery}</pre>
          </Debug>

          {documents.length === 1 && documents[0].clusterLead?.length > 0 && (
            <div style={{ display: 'flex', justifyContent: 'center', marginBottom: '2em' }}>
              <Link
                href={`/news/${encodeURIComponent(
                  uriToId(documents[0].clusterLead[0], { base: config.newsBaseUri })
                )}#${uriToId(documents[0].id, { base: config.newsBaseUri })}`}
              >
                <a style={{ textDecoration: 'none' }}>
                  <RoundedButton style={{ color: theme.colors.primary }}>
                    <b>
                      <BiNews style={{ top: '2px', position: 'relative' }} /> View Full Coverage
                    </b>
                  </RoundedButton>
                </a>
              </Link>
            </div>
          )}
          {documents.map((doc, index) => (
            <Fragment key={doc.id}>
              {index > 0 && (
                <Bubble>
                  {new Intl.DateTimeFormat('default', { day: 'numeric', month: 'short' })
                    .format(Date.parse(doc.dateCreated))
                    .toUpperCase()}
                </Bubble>
              )}
              <Card id={uriToId(doc.id, { base: config.newsBaseUri })}>
                <h1>
                  {doc.headline}{' '}
                  <Anchor href={`#${uriToId(doc.id, { base: config.newsBaseUri })}`}>#</Anchor>
                </h1>
                <Tags>
                  {doc.dateCreated && (
                    <Tag>
                      <TagIcon as={BiCalendar} />
                      <TagText>
                        {new Intl.DateTimeFormat('default', {
                          year: 'numeric',
                          month: 'numeric',
                          day: 'numeric',
                          hour: 'numeric',
                          minute: 'numeric',
                        }).format(Date.parse(doc.dateCreated))}
                      </TagText>
                    </Tag>
                  )}
                  {doc.location && (
                    <Tag>
                      <TagIcon as={BiGlobe} />
                      <TagText>{doc.location}</TagText>
                    </Tag>
                  )}
                  {Array.isArray(doc.schemas) && doc.schemas.length > 0 && (
                    <Tag>
                      <TagIcon as={BiPurchaseTag} />{' '}
                      <TagText>
                        {doc.schemas.map((schema, i) => (
                          <>
                            <Link
                              key={schema.id}
                              href={{ pathname: '/search', query: { schema: schema.id } }}
                            >
                              <a>{schema.label}</a>
                            </Link>
                            {i < doc.schemas.length - 1 ? ', ' : ''}
                          </>
                        ))}
                      </TagText>
                    </Tag>
                  )}
                </Tags>
                <Description
                  dangerouslySetInnerHTML={{
                    __html: annotate(underline(doc.description[0], doc), doc),
                  }}
                />
                <a href={doc.id} target="_blank" rel="noopener noreferrer">
                  <small>
                    <BiLink /> permalink
                  </small>
                </a>
                <Debug>
                  <pre>{JSON.stringify(doc, null, 2)}</pre>
                </Debug>
              </Card>
            </Fragment>
          ))}
        </Content>
        <StickyBox offsetTop={40} offsetBottom={40}>
          <InfoBox lead={lead} documents={documents} />
        </StickyBox>
      </Container>
    </Layout>
  );
}

export async function getServerSideProps({ req, query }) {
  const { news, debugSparqlQuery } = await getNewsByUri(
    idToUri(query.newsId, { base: config.newsBaseUri })
  );

  if (!news) {
    return { props: { initialData: {} } };
  }

  const documents = [];

  // Push the main news to the list of documents
  documents.push(news);

  // Populate event cluster with list of news
  const relatedDocs = await getClusterDocumentsByNews(news.id);
  documents.push(...relatedDocs);

  for (let i = 0; i < documents.length; i += 1) {
    // Get annotations
    documents[i].annotations = await getAnnotationsByContext(documents[i].context);

    // Get dataification
    documents[i].data = await getWikidataByNews(documents[i].id);

    // Get sources
    documents[i].sources = await getSources(documents[i].identifier[0], absoluteUrl(req));

    // Get entities
    documents[i].entities = await getEntities(documents[i].identifier[0], absoluteUrl(req));

    // Get props
    documents[i].props = await getNewsWikiProps(documents[i].id);
  }

  // Sort documents by date, in descending order
  documents.sort((a, b) => Date.parse(b.dateCreated) - Date.parse(a.dateCreated));

  // Breadcrumbs
  const breadcrumbs = {};
  if (typeof query.s === 'string') {
    try {
      Object.assign(breadcrumbs, querystring.parse(Buffer.from(query.s, 'base64').toString()));
    } catch (err) {
      console.error(err);
    }
  }
  if (breadcrumbs.subjects) {
    const subjectsCodes = await getSubjectCodes();
    const subject = subjectsCodes.find((subj) => subj.sc === breadcrumbs.subjects);
    breadcrumbs.subjects = subject?.prefLabel || null;
  }
  if (breadcrumbs.schema) {
    const schema = await getSchemaById(breadcrumbs.schema);
    breadcrumbs.schema = schema?.prefLabel || null;
  }

  return {
    props: {
      documents,
      lead: news,
      breadcrumbs,
      debugSparqlQuery,
    },
  };
}
