import App from 'next/app';
import { ThemeProvider } from 'styled-components';
import { Provider as ReakitProvider } from 'reakit';
import { Reset } from 'styled-reset';
import Head from 'next/head';

import NProgress from '@components/NProgress';
import GlobalStyle from '@styles/global';
import theme from '~/src/theme';

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;

    return (
      <ThemeProvider theme={theme}>
        <ReakitProvider>
          <Reset />
          <GlobalStyle />
          <Head>
            <meta charSet="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="icon" href="/images/favicon.png" type="image/png" />
            <link rel="shortcut icon" href="/images/favicon.png" type="image/png" />
          </Head>
          <NProgress />
          <Component {...pageProps} />
        </ReakitProvider>
      </ThemeProvider>
    );
  }
}

export default MyApp;
