import styled from 'styled-components';
import Head from 'next/head';
import Link from 'next/link';
import { Button } from 'reakit';
import { BiSearch } from 'react-icons/bi';

import breakpoints from '@styles/breakpoints';
import Layout from '@components/Layout';
import SearchInput from '@components/SearchInput';
import NextForm from '@components/NextForm';

const Hero = styled.div`
  width: 100%;
  min-height: calc(
    100vh - ${({ theme }) => `${theme.header.height} - ${theme.header.borderBottomWidth}`}
  );
  position: relative;

  display: flex;
  flex-direction: column;
`;

const HeroTop = styled.div`
  height: calc(
    (100vh - ${({ theme }) => `${theme.header.height} - ${theme.header.borderBottomWidth}`}) / 2
  );
  min-height: fit-content;
  display: flex;
  flex-direction: column-reverse;
  justify-content: center;
  align-items: center;
  width: 100%;
  background-color: #fff;
  position: relative;
  background-image: url();
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;

  ${breakpoints.tablet`
    flex-direction: row;
  `}
`;

const Title = styled.h1`
  width: 80%;
  text-align: center;
  ${breakpoints.tablet`
    width: 50%;
    height: auto;
    font-size: 3.5rem;
    text-align: left;
  `}
  ${breakpoints.desktop`
    font-size: 4rem;
  `}
`;

const Logo = styled.div`
  background-image: url(/search-engine/logo.png);
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center;
  width: 50%;
  height: 50%;
  max-width: 350px;
  ${breakpoints.tablet`
    height: 100%;
  `}
`;

const HeroMiddle = styled.div`
  width: 100%;
  height: 3.5em;
  margin-top: -28px;
  display: flex;
  justify-content: center;
`;

const HeroBottom = styled.div`
  min-height: calc(
    (100vh - ${({ theme }) => `${theme.header.height} - ${theme.header.borderBottomWidth}`}) / 2
  );
  margin-top: -28px;
  padding-bottom: 1em;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  background-color: #eee;
`;

const Subtitle = styled.span`
  display: block;
  font-size: 2rem;
  color: #787878;
  margin-top: 1em;
`;

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  width: 100%;
  overflow: auto;
  padding: 0 0.5em;
  flex: 0.5;
`;

const BigButton = styled.a`
  background-color: ${({ background }) => background};
  text-decoration: none;
  color: ${({ color }) => color};
  text-transform: uppercase;
  padding: 0.75em;
  text-align: center;
  border-radius: 8px;
  font-size: 2em;
  margin: 10px 20px;
  &:hover {
    color: ${({ color }) => color};
    text-decoration: underline;
  }
`;

const SearchForm = styled(NextForm)`
  display: flex;
  align-items: center;
  flex-grow: 1;
  max-width: 90%;
  background-color: rgba(255, 255, 255, 0.95);
  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.12),
    0 1px 5px 0 rgba(0, 0, 0, 0.2);
  position: relative;
  height: 100%;
  position: relative;
  ${breakpoints.tablet`
    max-width: 70%;
  `}
`;

const StyledSearchInput = styled(SearchInput)`
  flex: 1;

  .react-autosuggest__input {
    appearance: none;
    background-color: transparent;
    border: none;
    font-size: 1.2rem;
    letter-spacing: 0.1rem;
    padding: 0 10px;
    min-width: 0;
    width: 100%;
    outline: 0;
  }

  .react-autosuggest__container--open .react-autosuggest__suggestions-container {
    border: none;
    max-width: auto;
    min-width: 100%;
    right: auto;
    left: 0;
    top: 45px;

    background-color: #ffffff;
    outline: 0;
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.12),
      0 1px 5px 0 rgba(0, 0, 0, 0.2);
    color: #212121;
    border-bottom-left-radius: 0.25rem;
    border-bottom-right-radius: 0.25rem;
  }
`;

const SearchButton = styled(Button)`
  display: flex;
  justify-content: center;
  align-items: center;
  appearance: none;
  border: none;
  height: 80%;
  background-color: ${({ theme }) => theme.colors.primary};
  color: #fff;
  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.12),
    0 1px 5px 0 rgba(0, 0, 0, 0.2);
  margin-right: 10px;
  width: 50px;
  border-radius: 4px;
  cursor: pointer;
  ${breakpoints.tablet`
    width: 80px;
  `}
`;

const HomePage = () => {
  return (
    <Layout search={false}>
      <Hero>
        <HeroTop>
          <Title>Explore enriched news</Title>
          <Logo />
        </HeroTop>
        <HeroMiddle>
          <Link href="/search" passHref>
            <SearchForm method="GET">
              <StyledSearchInput
                name="q"
                placeholder="Search for subjects, persons, locations, dates, keywords, ..."
              />
              <SearchButton aria-label="Search" type="submit">
                <BiSearch style={{ width: 24, height: 24 }} />
              </SearchButton>
            </SearchForm>
          </Link>
        </HeroMiddle>
        <HeroBottom>
          <Subtitle>or explore using</Subtitle>
          <ButtonsContainer>
            <Link href="/search" passHref>
              <BigButton background="#c6c6c6" color="#000000">
                Event Schemas
              </BigButton>
            </Link>
          </ButtonsContainer>
        </HeroBottom>
      </Hero>
    </Layout>
  );
};

export default HomePage;
