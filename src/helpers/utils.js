/**
 * Gets the last part of an URI
 * Example:
 *  uriToId('http://dbpedia.org/page/Tim_Berners-Lee', { base: 'http://dbpedia.org/page' }) returns 'Tim_Berners-Lee'
 */
export function uriToId(uriPart, { base }) {
  const id = typeof base !== 'undefined' ? uriPart.substr(base.length + 1) : uriPart;
  return id;
}

/**
 * Converts an ID back to an URI, given a base
 * Example:
 *  idToUri('Tim_Berners-Lee', { base: 'http://dbpedia.org/page' }) returns 'http://dbpedia.org/page/Tim_Berners-Lee'
 */
export function idToUri(id, { base }) {
  const uri = typeof base !== 'undefined' ? `${base}/${id}` : id;
  return uri;
}

export function absoluteUrl(req, localhostAddress = 'localhost:3000') {
  let host = '';
  if (typeof window !== 'undefined' && window.location) {
    host = window.location.host;
  } else if (req && req.headers) {
    host = req.headers.host;
  } else {
    host = localhostAddress;
  }
  let protocol = /^localhost(:\d+)?$/.test(host) ? 'http:' : 'https:';

  if (
    req &&
    req.headers['x-forwarded-host'] &&
    typeof req.headers['x-forwarded-host'] === 'string'
  ) {
    host = req.headers['x-forwarded-host'];
  }

  if (
    req &&
    req.headers['x-forwarded-proto'] &&
    typeof req.headers['x-forwarded-proto'] === 'string'
  ) {
    protocol = `${req.headers['x-forwarded-proto']}:`;
  }

  return `${protocol}//${host}`;
}

export function capitalize([first, ...rest]) {
  return `${first.toUpperCase()}${rest.join('').toLowerCase()}`;
}

export function throttle(callback, limit) {
  let waiting = false;
  let latestCallThis = null;
  let latestCallArgs = null;
  const throttledFunction = (...args) => {
    if (waiting) {
      latestCallThis = this;
      latestCallArgs = args;
    } else {
      callback.apply(this, args);
      waiting = true;
      setTimeout(() => {
        waiting = false;
        if (latestCallThis !== null) {
          throttledFunction.apply(latestCallThis, latestCallArgs);
          latestCallThis = null;
          latestCallArgs = null;
        }
      }, limit);
    }
  };
  return throttledFunction;
}

export function stringToColor(str) {
  let hash = 0;
  if (str.length === 0) return hash;
  for (let i = 0; i < str.length; i += 1) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
    hash &= hash;
  }
  let color = '#';
  for (let i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 255;
    color += `00${value.toString(16)}`.substr(-2);
  }
  return color;
}

export function escapeRegex(string) {
  return string.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
}
