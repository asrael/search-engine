import cache from '@helpers/cache';
import querystring from 'querystring';

export const getPrefixes = () => `
  PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
  PREFIX schema: <http://schema.org/>
  PREFIX rnews: <http://iptc.org/std/rNews/2011-10-07#>
  PREFIX asrael: <http://asrael.eurecom.fr/asrael#>
  PREFIX nif: <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#>
  PREFIX itsrdf: <http://www.w3.org/2005/11/its/rdf#>
  PREFIX wdt: <http://www.wikidata.org/prop/direct/>
`;

export const doQuery = async (
  sparqlQuery,
  { endpoint = 'https://asrael.eurecom.fr/sparql' } = {}
) => {
  const data = await cache.exists(sparqlQuery).then(async (reply) => {
    let res;
    if (reply !== 1) {
      console.debug(sparqlQuery);

      res = await (
        await fetch(endpoint, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: querystring.stringify({
            query: sparqlQuery,
          }),
          redirect: 'follow',
        })
      ).json();

      // Cache the response
      await cache.set(sparqlQuery, JSON.stringify(res));
    } else {
      // Use cached response when available
      res = JSON.parse(await cache.get(sparqlQuery));
    }
    return res;
  });

  return data;
};

export default {
  getPrefixes,
  doQuery,
};
