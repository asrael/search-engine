export default function underline(text, news) {
  if (!news || !news.sources) {
    return text;
  }
  const sentencesArray = news.sources.source_sentences;
  if (sentencesArray && text) {
    let paragraphs = text.match(/<p>.*?<\/p>/g);
    sentencesArray.forEach((sentence) => {
      paragraphs = paragraphs.map((p) => {
        const sentenceOffset = p.indexOf(sentence.text.trim().replace(/ "$/, ''));
        if (sentenceOffset > -1 && Array.isArray(sentence.sources)) {
          sentence.sources.forEach((source) => {
            const tooltip = source.value
              ? source.value.replace(/[\u00A0-\u9999<>&]/gim, (i) => {
                  return `&#${i.charCodeAt(0)};`;
                })
              : '';

            const sourceOffset = sentence.text.indexOf(source.text);

            if (sourceOffset > -1) {
              const val = sentence.text.substr(sourceOffset, source.end - source.start);

              p = p.replace(
                new RegExp(`(?![^<>]*>)(\\W+)(${val})(\\W+)`, 'gi'),
                `$1<span asrael class="highlight ${
                  source.type === 'SOURCE-PRIM' ? 'primary' : 'secondary'
                }"${source.value ? ` title="${tooltip}"` : ``}>${val}</span>$3`
              );
            } else {
              console.warn('Unknown source offset for', source, sentence);
            }
          });
        }
        return p;
      });
    });
    return paragraphs.join('');
  }
  return text;
}
