import { STATUS_CODES } from 'http';

export class HTTPError extends Error {
  constructor(code, message, extras) {
    super(message || STATUS_CODES[code]);
    if (arguments.length >= 3 && extras) {
      Object.assign(this, extras);
    }
    this.statusCode = code;
  }
}

export async function validateRequest(req, res, options = {}) {
  if (typeof options.allowedMethods !== 'undefined') {
    const allowedMethods = Array.isArray(options.allowedMethods)
      ? options.allowedMethods
      : [options.allowedMethods].filter((x) => x);
    if (!allowedMethods.map((v) => v.toLowerCase()).includes(req.method.toLowerCase())) {
      throw new HTTPError(405, 'Method not allowed');
    }
  }
}

export function withRequestValidation(options = {}) {
  return function Extend(WrappedFunction) {
    return async (req, res) =>
      validateRequest(req, res, options)
        .then(() => WrappedFunction(req, res))
        .catch((err) => {
          const statusCode = err.statusCode || 500;
          res.status(statusCode).json({
            error: {
              status: err.statusCode,
              message: err.message,
            },
          });
        });
  };
}
