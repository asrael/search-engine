import { stringToColor, escapeRegex } from '@helpers/utils';

export default function annotate(text, news) {
  if (news && news.data) {
    news.data.forEach(({ start, end, label, value }) => {
      const val = escapeRegex(value);
      text = text.replace(
        new RegExp(`(?![^<>]*>)(\\W+)(${val})(\\W+)`, 'gi'),
        `$1<span asrael class="annotation" style="color: ${stringToColor(
          value
        )};" title=${JSON.stringify(label)}> ${value} </span>$3`
      );
    });
  }

  return text;
}
