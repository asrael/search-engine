export const getAnnotationColor = (annotationType) => {
  switch (annotationType) {
    case 'http://adel.eurecom.fr/Stanford/Person':
    case 'http://adel.eurecom.fr/Stanford/PERSON':
      return 'person';
    case 'http://adel.eurecom.fr/Stanford/Location':
    case 'http://adel.eurecom.fr/Stanford/LOCATION':
      return 'location';
    case 'http://adel.eurecom.fr/Stanford/Misc':
    case 'http://adel.eurecom.fr/Stanford/MISC':
      return 'misc';
    case 'http://adel.eurecom.fr/Stanford/Organization':
    case 'http://adel.eurecom.fr/Stanford/ORGANIZATION':
      return 'organization';
    case 'http://adel.eurecom.fr/Stanford/Thing':
    case 'http://adel.eurecom.fr/Stanford/THING':
      return 'thing';
    case 'http://adel.eurecom.fr/Stanford/Event':
    case 'http://adel.eurecom.fr/Stanford/EVENT':
      return 'event';
    case 'http://adel.eurecom.fr/Stanford/Product':
    case 'http://adel.eurecom.fr/Stanford/PRODUCT':
      return 'product';
    default:
      return 'default';
  }
};

const calculateAdelOffset = (htmlText, startOffset, endOffset) => {
  // Adel is based on stripped html text
  const pattern = /<(?:(?! ))(?:"[^"]*"['"]*|'[^']*'['"]*|[^'">])+>/g;
  let match = pattern.exec(htmlText);
  let totalTagsLength = 0;

  while (match != null) {
    if (match.index > startOffset + totalTagsLength) {
      break;
    }

    totalTagsLength += match[0].length;

    match = pattern.exec(htmlText);
  }

  const start = startOffset + totalTagsLength;
  const end = endOffset + totalTagsLength;
  return {
    start,
    end,
    text: htmlText.substr(start, end - start),
  };
};

const normalizeAdelText = (text) => text.replace(/\n/g, ' ').replace(/<\/p> <p>/g, '</p>  <p>');

export default function colorize(text, annotations) {
  text = normalizeAdelText(text);

  if (annotations.length > 0) {
    const previousTitleOffset = 0;

    // Create a flat array of annotations
    let flatAnnotations = [];
    annotations.forEach((annotation) => {
      annotation.values.forEach((value) => {
        flatAnnotations.push(value);
      });
    });

    // Sort annotations by start index
    flatAnnotations = flatAnnotations.sort((a, b) => {
      if (a.start === b.start) return 0;
      return a.start > b.start ? 1 : -1;
    });

    flatAnnotations.forEach((value) => {
      const colorType = getAnnotationColor(value.type);

      const adelOffset = calculateAdelOffset(text, value.start, value.end);

      const words = text.substr(adelOffset.start, adelOffset.end - adelOffset.start);
      const closingTagIndex = words.indexOf('</');
      if (closingTagIndex >= 0) {
        // Hack: if Adel annotated words from two different paragraphs, ignore it
        // (eg. one word from 1st paragraph then one word from 2nd paragraph)
        return;
      }

      const replacement = `<a asrael href="${value.source}" target="_blank"><span asrael class="colorize color-${colorType}">${words}</span></a>`;
      text = text.substr(0, adelOffset.start) + replacement + text.substr(adelOffset.end);
    });
  }

  return text;
}
