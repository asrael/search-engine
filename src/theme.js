const theme = {
  fontFamily: {
    sansSerif:
      '-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif',
    mono:
      'Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace',
  },
  colors: {
    text: '#333',
    background: '#fff',
    primary: '#358189',
    secondary: '#893d35',
    linkHover: '#a02a0c',
    light: '#f5b1a1',
  },
  header: {
    height: '80px',
    borderBottomWidth: '1px',
  },
};

export default theme;
