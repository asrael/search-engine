# ASRAEl-SearchEngine

## Getting Started

1. Go to project folder and install dependencies:

   ```sh
   npm install
   ```

1. Launch development server, and open http://localhost:3000 in your browser:

   ```sh
   npm run dev
   ```

## Project Structure

```
dist/                        web app production build
src/                         project source code
|- components/               app components
|- pages/                    app pages
|- helpers/                  app helpers
|- styles/                   app global css variables and functions
|  |- theme.js               main theme file
public/                      app assets (images, fonts, ...)
```

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

## Docker

- Build for development:

  ```sh
  docker-compose -f docker-compose.yml -f docker-compose.dev.yml up
  ```

- Build for production:

  ```sh
  docker-compose -f docker-compose.yml -f docker-compose.prod.yml up
  ```
